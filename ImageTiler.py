import numpy as np
import scipy.misc
import cPickle
#from Preprocess import Faces
class ImageTiler():
    def __init__(self):
        #Obj1 = Faces.Faces()
        self.InputImages = self.readCpickleFile("ImageMeans32.p")
        #self.InputImages = Obj1.inputDataset
        self.OutputFilename = "Output.jpg"
        self.Colours = 1
        self.ActualImageSize = int(np.sqrt(self.InputImages.shape[1]/self.Colours))
        self.ImageSize = 100
        self.Rows = 2
        self.Columns = 6
        self.RGap = int(self.ImageSize*0.15)
        self.CGap = int(self.ImageSize*0.15)
        self.LargeRows = (self.ImageSize * self.Rows) + (self.RGap * ( self.Rows + 1 ) )
        self.LargeColumns = (self.ImageSize * self.Columns) + (self.CGap * ( self.Columns + 1 ) )
        print self.LargeColumns,self.LargeRows
        self.blank = np.ones(self.LargeRows*self.LargeColumns*self.Colours).reshape(self.LargeRows,self.LargeColumns,self.Colours) * 255
        #Should have self.Rows * self.Columns values to fill the space
        self.Indexes = np.arange(self.Rows*self.Columns) +(10 * 25)
        #for kmeans cifar
        # self.Indexes = np.array([120,998,1017,1231,1137,879,995,1217,190,385,840,112,1007,1219,1099,228,236,207,456,1275])
        #For k-max cifar
        #self.Indexes = np.array([687,686,739,557,28,879,995,1217,190,385,840,112,1007,1219,1099,228,236,207,456,1275])
        #For MNIST AA
        #self.Indexes = np.array([1021,31,856,878,424,689,1009,704,637,558,140,29])
        #For CBCL
        #self.Indexes = np.array([936,521,226,236,843,63,64,23,58,9,307,414])
        #Colour cifar
        #self.Indexes = np.array([45,36,148,188,266,192,886,948,816,135,297,595])
        #For deep faces
        #self.Indexes = np.array([0,25,26,18,92,193,274,282,540,562,612,685,982])+1
        #For grey CIFAR
        self.Indexes = np.array([960,883,855,777,690,587,582,550,436,370,242,238,263])

    def readCpickleFile(self, filename):
        """Read a Cpickle from HDD"""

        fileobj = open(str("Tiler/")+filename,'rb')
        data = cPickle.load(fileobj)
        fileobj.close()
        return data['means']

    def AsImage(self,x):
        """reshapes the input array as matrix image"""

        x = x.reshape(self.ActualImageSize,self.ActualImageSize,self.Colours)
        if(self.Colours == 1):
            x = scipy.misc.imresize(x.reshape(x.shape[:2]),np.array([self.ImageSize,self.ImageSize])).reshape(self.ImageSize,self.ImageSize,self.Colours)
        else:
            x = scipy.misc.imresize(x,np.array([self.ImageSize,self.ImageSize,self.Colours]))
        return x

    def SaveImage(self):
        #self.blank = self.addBorder(self.blank)
        if(self.Colours == 1):
            scipy.misc.imsave(str("Tiler/")+self.OutputFilename,self.blank.reshape(self.blank.shape[:2]))
        else:
            scipy.misc.imsave(str("Tiler/")+self.OutputFilename,self.blank)
        return None
    def addBorder(self,x):
        """add border to the image sent"""
        BorderColour = 0
        Crange = 1
        Rrange = 1
        if(self.Colours == 1):
            Border = np.ones(1).reshape(1,1,1) * BorderColour
        else:
            Border = np.ones(3).reshape(1,1,3) * BorderColour

        for i in xrange(self.LargeRows):
            for j in xrange(self.LargeColumns):
                if ((not(Rrange < i < (self.LargeRows -Rrange))) | (not(Crange < j < (self.LargeColumns -Crange)))):
                    x[i,j,:] = Border
        return x




    def Tile(self):
        """Tiles the images one by one and saves the image"""
        n = -1
        for i in xrange(self.Rows):
            for j in xrange(self.Columns):
                n = n + 1
                x = (i*self.ImageSize) + (self.RGap*i) + (self.RGap)
                y = (j*self.ImageSize) + (self.CGap*j) + (self.CGap)
                print x,y
                #self.blank[x:(x+self.ImageSize),y:(y+self.ImageSize),:]
                self.blank[x:(x+self.ImageSize),y:(y+self.ImageSize),:] = self.AsImage(self.InputImages[self.Indexes[n]])

        self.SaveImage()
        return None

    def read(self):
        self.InputImages = np.empty((50*50))
        for i in xrange(1000):
            print self.InputImages.shape
            filename = str('Tiler/Cluster/')+'patch 128.'+str(i)+str('.jpg')
            temp = scipy.misc.imread(filename).flatten()
            self.InputImages = np.vstack((self.InputImages,temp))
        return None

if __name__ == '__main__':

    Obj = ImageTiler()
    #Obj.read()
    Obj.Tile()

    print 'done'