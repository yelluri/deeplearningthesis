import numpy as np
import cPickle
from sklearn.preprocessing import MinMaxScaler,StandardScaler
from Preprocess import AutoEncode
from Globals import *

def getDataset(ScalingType = 'MinMax', ReducedDim = None):
    """Returns the training and test Dataset after Scaling it as Scecified
    Scale Types are: MinMax, Whitten
    ReducedDim should be set the the number of dimension to which we have to reduce the dimensions
    """

    trainSet = readCpickleFile("train.p")
    trainX = trainSet['input']
    trainY = trainSet['output'].flatten()
    del(trainSet)
    testSet = readCpickleFile("test.p")
    testX = testSet['input']
    testY = testSet['output'].flatten()
    del(testSet)
    if(ReducedDim != None):
        Obj = AutoEncode.AutoEncode(ReducedDim)
        Obj.train(trainX)
        Obj.encode(trainX,tmax=200)
        Obj.encode(testX,tmax=200)
        print "Reduced the dimensions to "+str(ReducedDim)
    trainX,testX = ScaleData(trainX,testX,'MinMax')

    return trainX, trainY, testX, testY


def readCpickleFile(filename):
    """Reads a Cpickle file from HDD"""

    fileobj = open(str(PATH)+filename,'rb')
    data = cPickle.load(fileobj)
    fileobj.close()

    return data

def ScaleData(trainX, testX, Type = 'MinMax'):
    """Scale the Data"""

    if(Type == 'MinMax'):
        Scaler = MinMaxScaler(feature_range=(-1,1))
    elif(Type == 'Whitten'):
        Scaler = StandardScaler()

    trainX = Scaler.fit_transform(trainX)
    testX = Scaler.transform(testX)

    return trainX,testX