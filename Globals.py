


#Base Patch size to which the K means has to run
PATCH_SIZE = 8
IMAGE_SIZE =32

#Number of Centroids to be computed
K_MEANS = {1:256,2:600,4:500,8:800,16:1000,32:1500,64:1050,128:1000}
COLOURS = 1


#No. of base patches to be extracted in Round or LOOP(NO_OF_LOOPS)
NO_OF_RANDOM_PATCHES = 1000
NO_OF_LOOPS = 100

#On which Dataset we should do the learning
DATASET = 'CIFAR10'
#DATASET = 'Faces'
#DATASET = 'CBLC'
#DATASET = 'MNIST'


#Learning on Whittened or unWhittened images
WHITTEN = False


#Type of Clustering
CLUSTER_TYPE = "KMAXOIDS"
#CLUSTER_TYPE = "KMEANS"

#Type of constrained convex Optimizer for image reconstruction
OPT = "FRANK_WOLFE"
#OPT = "PSEUDO_INVERSE"
#OPT = "CVXPY"


#Path to store the results in the HDD
PATH = "/home/vignesh/workspace/Results/"+str(DATASET)+"/"+str(CLUSTER_TYPE)+"/"


#Number of pathes to be extracted in each Round for higher levels of Patch size(First value is for the highest patch size and last value is for the smallest patch size)
#RUNS = [500,500,500,500]
RUNS = [1000,1000,1000,1000]

#For visualizing
VISUALIZE = False

#For creating the dataset object just once
from Preprocess import CIFAR10,Faces,CBLC,MNIST
if(DATASET == "Faces"):
    DATASETOBJ = Faces.Faces()
elif(DATASET == "CIFAR10"):
    DATASETOBJ = CIFAR10.CIFAR10()
elif(DATASET == "MNIST"):
    DATASETOBJ = MNIST.MNIST()
else:
    DATASETOBJ = CBLC.CBLC()