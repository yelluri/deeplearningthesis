import numpy as np
from Preprocess import CIFAR10,FileManager,Kaggle2,KMAXOIDS,KMEANS
from scipy import linalg
import scipy.misc
from random import randint
from Globals import *
from math import floor
import time
from sklearn.metrics.pairwise import pairwise_distances
import scipy.ndimage
from numpy import convolve
from skimage.color import rgb2gray   
from skimage.feature import match_template         


class CatDogDataset():
    def __init__(self):
        
        self.patchSize = 32
        self.fileObj = FileManager.FileManager()
        self.Image_size = 128                                                                
        self.Patch_size = 32
        self.critical_aspect_ratio = self.Patch_size / float(self.Image_size)
        self.patch_set = np.empty((0,3*self.Patch_size*self.Patch_size),int)                #To collect the patches extracted
        self.col_wise_total = floor(self.Image_size/self.Patch_size)                        #No. of patches possible to extract in a column
        self.row_wise_total = floor(self.Image_size/self.Patch_size)                        #No. of patches possible to extract in a row
        self.total_possible_patches = int(self.col_wise_total * self.row_wise_total)        #Total No. of patches that can be extracted from the image
        self.resultPath = PATH
        self.label = np.array(['cat','dog'])
        self.classLabel = "dog"        
        self.count = randint(0,10000)
        
    def _readImage(self, filename):
        """Reads the image from the given path and returns the image as a matrix"""
        
        Image = scipy.misc.imread(filename)
        Image = self._resizeImage(Image)
        
        return Image
    
    
    def _resizeImage(self, Image):
        """Resizes the given Image for having a normalized dimension image with same aspect ratio"""

        dim = Image.shape
        newDim = [0,0]

        if (dim[0]>dim[1]):                                                                 #dim0 is higher
            
            ratio = dim[1] / float(dim[0])
            if (ratio < self.critical_aspect_ratio):
                
                ratio = self.critical_aspect_ratio
            newDim[0] = self.Image_size
            newDim[1] = int(self.Image_size * ratio)
            
        else:                                                                               #dim1 is higher
            
            ratio = dim[0] / float(dim[1])
            if (ratio < self.critical_aspect_ratio):
                ratio = self.critical_aspect_ratio
            newDim[0] = int(self.Image_size * ratio)
            newDim[1] = self.Image_size 
        
        resizedImage = scipy.misc.imresize(Image,newDim)
        
        return resizedImage
    
    
    def _chooseNextImage(self):
        """Chooses an Image at random from the dataset"""
        
        flag = True
        while(flag):                                                                        # To check whether the image is highly rectangular 
                                                                                            #so that we can't get a patch
            fileName = "/home/vignesh/Datasets/Cats_Dogs/"+str(self.classLabel)+"."+str(self.count)+".jpg"
            Image = self._readImage(fileName)
            self.count = self.count + 1
            if (self.critical_aspect_ratio < Image.shape[0]/float(Image.shape[1]) < 1.0/self.critical_aspect_ratio):
                flag = False

        return Image.astype(np.float)


class Filter():
    def __init__(self):
        
        self.patchSize = 32
        self.fileObj = FileManager.FileManager()
        self.datasetObj = CatDogDataset()
        self.KMeans = 64
        self.means, self.w_means= self.fileObj.readMeans(self.patchSize)
        self.w_means = self.w_means[:self.KMeans]
        self.closestPatch = np.zeros_like(self.means)
        self.distanceArray = np.zeros(self.KMeans)-np.inf
        self.closestImage = np.empty(self.KMeans,dtype = 'a25')
        self.count = 0
        self.resultPath = "/home/vignesh/workspace/Results/closestPatch"
        self.previous_patch_size = self.patchSize/2
        
        
    def ImageSlicing(self,OrgImage):
        """For converting the image into set of image patches"""
        #For cropping unwanted pixels
        dim = OrgImage.shape
        OrgImage = OrgImage[0:(0+(dim[0]/self.patchSize)*self.patchSize),0:(0+(dim[1]/self.patchSize)*self.patchSize),:]
        dim = OrgImage.shape
        self.patch_set = np.empty(( 0 , 3 * self.patchSize * self.patchSize ) , float )
        for i in xrange(int((dim[0]-self.patchSize)/float(self.previous_patch_size))+1):                #For extracting the patches like a sliding window
            for j in xrange(int((dim[1]-self.patchSize)/float(self.previous_patch_size))+1):
                row = i*self.previous_patch_size
                col = j*self.previous_patch_size
                patch = OrgImage[row:(row+self.patchSize),col:(col+self.patchSize),:]         #Extracting the patch in Orginial Image
                self.patch_set = np.vstack((self.patch_set,patch.flatten()))
        
        
        return self.patch_set
    
    
    def applyFilter(self,Image):
        """Applies filter for the given image"""
        patches = self.ImageSlicing(Image)
        #dist_matrix = np.dot(self.w_means,patches.T)
        for i in xrange(self.KMeans):
            for j in xrange(patches.shape[0]):

#                 temp = rgb2gray(patches[j].reshape(self.patchSize,self.patchSize,3))
#                 temp = self.normVector(temp)
#                 w_temp = self.normVector(rgb2gray(self.w_means[i].reshape(self.patchSize,self.patchSize,3)))
                #temp = temp.reshape(self.patchSize,self.patchSize,3)
                #w_temp = w_temp.reshape(self.patchSize,self.patchSize,3)
                #assert 0,rgb2gray(self.w_means[i].reshape(self.patchSize,self.patchSize,3))
                #filteredImage = scipy.ndimage.filters.convolve(temp,w_temp,mode='reflect')
                filteredImage = match_template(rgb2gray(patches[j].reshape(self.patchSize,self.patchSize,3)),rgb2gray(self.w_means[i].reshape(self.patchSize,self.patchSize,3)))
                #assert 0,filteredImage.sum()
#                 scipy.misc.imsave('f0.jpg', scipy.misc.imresize(temp,(50,50)))
#                 scipy.misc.imsave('f1.jpg', scipy.misc.imresize(w_temp,(50,50)))
#                 scipy.misc.imsave('f2.jpg', scipy.misc.imresize(filteredImage   ,(50,50)))
#                 assert 0,filteredImage.sum()
                similarity = filteredImage.sum()
                if self.distanceArray[i] < similarity:
                    self.distanceArray[i] = similarity
                    self.closestPatch[i][:] = patches[j][:]
                    #self.closestImage[i] = self.datasetObj.classLabel + "." + str(self.datasetObj.count-1)

        return None
        
        
        
    def findClosestPatch(self,loops):
        """Finds the closest patch to the whittned means which is used a filter"""
        for i in xrange(loops):
            print i
            Image = self.datasetObj._chooseNextImage()
            self.applyFilter(Image)
            self.plot()
        
        return None
    
    def plot(self):
        """save the images"""
        path = str(self.resultPath)+"patches/"
        self.fileObj.validateFilePath(path)
        for i in xrange(self.KMeans):
            scipy.misc.imsave(path+"patch"+str(i)+"w.jpg", scipy.misc.imresize(self.w_means[i].reshape(self.patchSize,self.patchSize,3),(50,50)))
            scipy.misc.imsave(path+"patch"+str(i)+".jpg", scipy.misc.imresize(self.closestPatch[i].reshape(self.patchSize,self.patchSize,3),(50,50)))
            #scipy.misc.imsave(path+"patch"+str(i)+"o.jpg", self.getImage(self.closestImage[i]))
        return None
    
    def getImage(self,name):
        """Returns the image with the given filename"""
        
        fileName = "/home/vignesh/Datasets/Cats_Dogs/"+str(name)+".jpg"
        Image = self.datasetObj._readImage(fileName)
        return Image
    
    
    def normVector(self, vector):
        return vector #/ np.linalg.norm(vector)
    
if __name__ == "__main__":
    
    Obj = Filter()
    t1 = time.time()
    count = 1000
    Obj.findClosestPatch(count)
    Obj.datasetObj.classLabel = "cat"
    Obj.datasetObj.count =0
    Obj.findClosestPatch(count)
    print time.time() - t1
    
