from Preprocess import Image_Analyser as IA,CIFAR10 as Cif,FileManager,Kaggle2 as Kag,Model
import numpy as np
import time
from Globals import *




if __name__ == "__main__":

    l = RUNS
    t1 = time.time()
    cifObj = DATASETOBJ
    Obj = IA.Image_Analyser()
    fileObj = FileManager.FileManager()



    #Computing the prototypes at base layer
    Obj.extractPatchesAtRandom()
    Obj.computeCentroids()

    #Computing prototypes of increasing patch size at middle layers
    print time.time()-t1
    patchSize = PATCH_SIZE
    print IMAGE_SIZE,patchSize
    while(IMAGE_SIZE > patchSize):
        patchSize = patchSize * 2
        print patchSize
        Obj.reconstructAndExtractPatch(patchSize, l.pop(),60)
        print time.time()-t1


    #For the final patch i.e the entire image itself
    Obj.reconstructAndExtractPatch( patchSize * 2, l.pop(), 60)



