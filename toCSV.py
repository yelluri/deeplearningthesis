'''
Created on Feb 29, 2016

@author: vignesh
'''

import numpy as np
from Globals import *
from Preprocess import CIFAR10,FileManager,Kaggle2,KMAXOIDS,KMEANS
import cPickle
import scipy.misc
import gc
import csv


class toCSV():
    """Convert the dataset from cPickle file to CSV file"""

    def __init__(self):
        '''
        Constructor
        '''
        self.fileObj = FileManager.FileManager()
        self.resultPath = PATH
        self.Base_Patch_size = PATCH_SIZE
        self.colours = COLOURS
        self.KMeans = K_MEANS
        self.datasetObj = DATASETOBJ
        self.Image_size = IMAGE_SIZE

    def readCpickleFile(self, filename):
        """Read a Cpickle from HDD"""

        fileobj = open(str(self.resultPath)+filename,'rb')
        data = cPickle.load(fileobj)
        fileobj.close()
        return data

    def getTitel(self):
        """Title for each column"""
        title = []
        for i in xrange(self.KMeans[self.Image_size*2]):
            title.append("Arch"+str(i))
        title.append("Label")
        return np.array(title)

    def writeCSVFile(self, data, filename):
        """writes the given CSV file in HDD"""

        #np.savetxt(str(self.resultPath)+filename, data, delimiter=",")
        title = self.getTitel().reshape(1,self.KMeans[self.Image_size*2]+1)
        print title.shape
        with open(str(self.resultPath)+filename, "wb") as f:
            writer = csv.writer(f)
            writer.writerows(title)
            writer.writerows(data)
        return None


    def convert(self):
        """Converts the array and puts it in a CSV file"""

        trainSet = self.readCpickleFile("train.p")
        X = trainSet['input']
        Y = trainSet['output']
        X = np.hstack((X,Y))
        del(trainSet)
        self.writeCSVFile(X,"train.csv")
        testSet = self.readCpickleFile("test.p")
        X = testSet['input']
        Y = testSet['output']
        X = np.hstack((X,Y))
        del(testSet)
        self.writeCSVFile(X,"test.csv")
        print "Done"
        return None


if __name__ == "__main__":

    Obj = toCSV()
    Obj.convert()