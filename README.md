This repository is for Master Thesis on Deep Learning using Archetypal Analysis Hierarchically.


To start learning run main.py

configurations can be changed in GLOBALS.py

The core logic is in Preprocess/Image_Analyser.py

Used on CIFAR10, Cats and Dogs(Kaggle), CBCL Faces, MNIST Digits Datasets.

Have to change the dataset and result paths accordingly when using in a new system.
