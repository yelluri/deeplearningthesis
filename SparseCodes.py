'''
Created on Mar 04, 2016

@author: vignesh
'''

import numpy as np
from Globals import *
from Preprocess import CIFAR10,FileManager,Kaggle2,KMAXOIDS,KMEANS,AutoEncode,classicESN
import cPickle
import scipy.misc
import gc
import shutil
from keras.utils import np_utils
from sklearn.ensemble import RandomForestClassifier,AdaBoostClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.grid_search import GridSearchCV
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis,QuadraticDiscriminantAnalysis
from sklearn.metrics import classification_report,accuracy_score
from sklearn import svm
from sklearn.svm import LinearSVC
from sklearn.decomposition import PCA
from sklearn.preprocessing import MinMaxScaler,StandardScaler
import os

class SparseCodes():
    """Convert the dataset from cPickle file to CSV file"""

    def __init__(self):
        '''
        Constructor
        '''
        self.fileObj = FileManager.FileManager()
        self.resultPath = PATH
        self.Base_Patch_size = PATCH_SIZE
        self.colours = COLOURS
        self.Kmeans = K_MEANS
        self.datasetObj = DATASETOBJ
        self.Image_size = IMAGE_SIZE
        self.level = self.Image_size  #* 2
        self.meanImages = self.fileObj.readAllMeanImages(self.Image_size)[self.level/2]



    def readCpickleFile(self, filename):
        """Read a Cpickle from HDD"""

        fileobj = open(str(self.resultPath)+filename,'rb')
        data = cPickle.load(fileobj)
        fileobj.close()
        return data


    def shuffleData(self,Input,Output):
        """Shuffles the dataset"""
        shuffler = np.arange(Input.shape[0])
        np.random.shuffle(shuffler)
        return Input[shuffler],Output[shuffler]

    def DistributeData(self,X,Y):
        """Distribute the Classes somewhat equally in the train set and remaining in the test set"""

        shuffler = np.argsort(Y)
        X = X[shuffler]
        Y = Y[shuffler]
        faces = np.where(Y==1.0)
        nFaces = np.where(Y==-1.0)
        facesX = X[faces]
        facesY = Y[faces]
        nFacesX = X[nFaces]
        nFacesY = Y[nFaces]
        del(X)
        del(Y)
        facesX,facesY = self.shuffleData(facesX,facesY)
        nFacesX,nFacesY = self.shuffleData(nFacesX,nFacesY)
        # scipy.misc.imsave('Im.jpg',self.VisualizeVector(facesX[0]).reshape(32,32))
        # assert 0,1
        faceTotal = 2100
        nFaceTotal = 4500
        trainX = np.vstack((facesX[:faceTotal],nFacesX[:nFaceTotal]))
        trainY = np.append(facesY[:faceTotal],nFacesY[:nFaceTotal]).flatten()
        testX = np.vstack((facesX[faceTotal:],nFacesX[nFaceTotal:]))
        testY = np.append(facesY[faceTotal:],nFacesY[nFaceTotal:]).flatten()
        facesX = faces = facesY = nFacesY = nFaces = nFacesX = 0                        #To clear the variables from the RAM
        print trainX.shape,trainY.shape,testX.shape,testY.shape
        return trainX,trainY,testX,testY


    def getData(self):
        """Reads the sparse codes which are nothing but the convex coe-efficients of the Archetypes from HDD"""

        # trainSet = self.readCpickleFile("train.p")
        # self.trainX = trainSet['input']
        # self.trainY = trainSet['output'].flatten()
        # print self.trainX.shape
        # del(trainSet)
        # testSet = self.readCpickleFile("test.p")
        # self.testX = testSet['input']
        # self.testY = testSet['output'].flatten()
        # del(testSet)

        self.trainX,self.trainY = self.datasetObj._readDataset()
        self.testX, self.testY = self.datasetObj._readTestSet()



        # self.testX = np.vstack((self.testX,self.trainX))
        # self.testY = np.append(self.testY,self.trainY)
        # del(self.trainX)
        # print 'Merged'
        # self.trainX,self.trainY,self.testX,self.testY = self.DistributeData(self.testX,self.testY)
        # print 'Distributed'
        # # #self.DoPCA(Dim=1000)


        self.trainX,self.trainY = self.shuffleData(self.trainX,self.trainY)
        self.testX, self.testY = self.shuffleData(self.testX, self.testY)



        # temp = 9874
        # Im = self.VisualizeVector(self.testX[temp]).reshape(32,32)
        # print self.testY[temp]
        # scipy.misc.imsave("0Feature.jpg",Im)
        # self.testX, self.testY = self.datasetObj._readTestSet()
        # Im = self.testX[temp].reshape(32,32)
        # scipy.misc.imsave("0Image.jpg",Im)
        # print self.testY[temp]
        # assert 0,1


        # self.trainX,self.trainY = self.datasetObj._readDataset()
        # self.testX, self.testY = self.datasetObj._readTestSet()
        # self.testX = np.vstack((self.testX,self.trainX))
        # self.testY = np.append(self.testY,self.trainY)
        # self.trainX,self.trainY = self.testX,self.testY
        #self.ScaleData("unitVar")
        return None

    def DoPCA(self,Dim = 800):
        """Do Dimensionality reduction using PCA"""

        Obj = PCA(n_components=Dim)
        self.trainX = Obj.fit_transform(self.trainX)
        self.testX = Obj.transform(self.testX)
        print 'PCA Done'
        return None


    def ArchtypalEncode(self,Dim = 800):
        """Do Auto encoding with Archtypal analysis"""

        AE = AutoEncode.AutoEncode(maxoids= Dim)
        AE.train(self.trainX)
        print 'Found Extremal Points'
        self.trainX = AE.encode(self.trainX)
        print 'Encoded Train set'
        self.testX = AE.encode(self.testX)
        print 'Encoded'
        return None

    def ScaleData(self,Type = 'MinMax'):
        """Scale the Data"""

        if(Type == 'MinMax'):
            Scaler = MinMaxScaler(feature_range=(0.0,1.0))
        elif(Type == 'unitVar'):
            Scaler = StandardScaler()

        self.trainX = Scaler.fit_transform(self.trainX)
        self.testX = Scaler.transform(self.testX)

        return None
    def classifiedExamples(self,predictY):
        """See the images classified correctly and wrongly"""

        tempPath = self.resultPath + "Misclassified/"
        if os.path.exists(tempPath): #Clear the directory in which you will visualize the misclassified Examples
            shutil.rmtree(tempPath)
        if(DATASET == "MNIST"):
            #Since no shuffling of the test set in MNIST, i see the org image and transformed image with same search index
            ImageX, ImageY = self.datasetObj._readTestSet()
        for i in np.unique(predictY).flatten():
            print i
            mistake = np.array(np.where((predictY != self.testY) & (predictY == i))).flatten()
            for j in mistake:
                Image = self.VisualizeVector(self.testX[j])
                self.fileObj.saveImage(self.fileObj.validateFilePath(tempPath+str(i)+"/")+str(j)+"."+str(int(self.testY[j]))+".F.jpg", self.fileObj.resizeImage(Image.reshape(self.Image_size,self.Image_size,self.colours),(50,50)))
                if(DATASET == "MNIST"):
                    self.fileObj.saveImage(self.fileObj.validateFilePath(tempPath+str(i)+"/")+str(j)+"."+str(int(ImageY[j]))+".I.jpg", self.fileObj.resizeImage(ImageX[j].reshape(self.Image_size,self.Image_size,self.colours),(50,50)))

        return None

    def VisualizeVector(self,Feature):
        """Multiply the convex co-effecients with the Mean Images to see the new Image"""
        dim = np.array([self.Image_size,self.Image_size])
        PatchSize = self.level/2
        if(self.Image_size * 2 > self.level):
            """This processing is only for all layers which works on image patches and not on the final image as a whole"""
            centroid = Feature.reshape(4,self.Kmeans[self.level/2]).T           #Column stockhastic matrix
            newImage = np.dot(self.meanImages.T,centroid).T
            newImage = np.swapaxes(newImage.reshape((dim[0]/PatchSize)*(dim[1]/PatchSize),PatchSize,PatchSize,self.colours).reshape((dim[0]/PatchSize),(dim[1]/PatchSize),PatchSize,PatchSize,self.colours),1,2)
            newImage = newImage.reshape((dim[0]/PatchSize)*PatchSize,(dim[1]/PatchSize)*PatchSize,self.colours)

        else:
            #This case is for visualizing the last layer weights or the weights of the entire image size
            centroid = Feature.reshape(self.Kmeans[self.level/2],1)
            newImage = np.dot(self.meanImages.T, centroid)
        return newImage

    def Classify(self):
        """Classify using a machine learning method"""

        #predictY = self.knn(40,self.trainX,self.trainY,self.testX)
        #predictY = self.RandomForRest()
        predictY = self.SVM()
        #predictY = self.AdaBoost()
        #predictY = self.GridSearch()
        #predictY = self.LDA()
        #predictY = self.QDA()
        #predictY = self.ESN()
        #predictY = self.LabelledArchetypes()
        #self.classifiedExamples(predictY)
        print classification_report(self.testY,predictY)
        print accuracy_score(self.testY,predictY)

        return None


    def LabelledArchetypes(self):
        """Label the Archetypes from using the training examples and predict a test point with the class of closest Archetype"""
        print 'Classifying using Archetpyes'
        self.Archetypes = self.fileObj.readAllMeans(self.level)[self.level]
        self.ArchLabels = self.knn(3,self.trainX,self.trainY,self.Archetypes).flatten() #Label Archtypes
        return self.knn(1,self.Archetypes,self.ArchLabels,self.testX).flatten()



    def ESN(self):
        """Echo state network"""

        self.trainY[np.where(self.trainY == -1.0)] = 0.0
        self.testY[np.where(self.testY == -1.0)] = 0.0
        self.trainY = np_utils.to_categorical(self.trainY, self.datasetObj.Total_classes)
        #self.testY = np_utils.to_categorical(self.testY, self.datasetObj.Total_classes)
        ESNObj = classicESN.Reservoir(self.trainX,self.trainY,size=800)
        print "training ESN"
        ESNObj.trainReservoir()
        print "Training completed !!!"
        labels = ESNObj.predict(self.testX)
        return labels*1.

    def QDA(self):
        """Quadratic Discriminent Analysis"""
        print "QDA"
        QDAObj = QuadraticDiscriminantAnalysis()
        QDAObj.fit(self.trainX,self.trainY)

        return QDAObj.predict(self.testX)

    def LDA(self):
        """Linear Discriminent Analysis"""

        print 'Linear Discriminant Analysis'
        LDAObj = LinearDiscriminantAnalysis()
        LDAObj.fit(self.trainX,self.trainY)

        return LDAObj.predict(self.testX)


    def GridSearch(self):
        """Do grid search for Parameter optimization"""
        forest = RandomForestClassifier()
        print "RF"
        paramater_grid = dict(n_estimators = range(60,90),random_state = [5],criterion = ['gini'],warm_start = [False],max_features = ["auto","sqrt"])

        grid = GridSearchCV(forest,paramater_grid,cv=3,scoring='accuracy',n_jobs=-1,verbose=1)
        grid.fit(self.trainX, self.trainY)
        print grid.best_score_
        print grid.best_params_

        return grid.predict(self.testX)



    def AdaBoost(self):
        AdaObj = AdaBoostClassifier()
        AdaObj.fit(self.trainX,self.trainY)
        return AdaObj.predict(self.testX)


    def SVM(self):
        """Apply SVM"""
        print 'SVM'
        SVMObj = svm.LinearSVC(verbose=True)
        #SVMObj = svm.NuSVC(nu=0.4,verbose=True)
        SVMObj.fit(self.trainX,self.trainY)
        return SVMObj.predict(self.testX)

    def RandomForRest(self):
        """Apply Random Forest Algorithm for Classification"""
        print "non grid rf"
        RFObj = RandomForestClassifier(n_estimators=250,verbose=True,n_jobs=-1)
        RFObj.fit(self.trainX,self.trainY)
        return RFObj.predict(self.testX)

    def knn(self,k,trainX,trainY,testX):
        """Train and predict with Knn classifier"""
        print 'knn'
        print k
        KnnObj = KNeighborsClassifier(n_neighbors=k,n_jobs = -1)
        KnnObj.fit(trainX, trainY)
        return KnnObj.predict(testX)


if __name__ == "__main__":

    Obj = SparseCodes()
    Obj.getData()
    Obj.Classify()