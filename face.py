import numpy as np
import glob
import scipy.misc
import cPickle
import time
import gc

class vectorize():
    def __init__(self):
        self.DatasetPath = '/home/vignesh/Datasets/faces-aligned/*.jpg'     #This path means all filenames in this path with .jpg extension
        self.Imagesize = 128                                                #We used to work with square images and the size of that square is specified
        self.Dataset = np.empty((0,self.Imagesize**2),float)                #All the images are flattened and stored in this variable
        self.ImageDim = np.array([self.Imagesize,self.Imagesize])
        
    def ProcessImages(self):
        """Reads each image in the given specified folder and process them"""
        count = 0
        volume = 0
        for filename in glob.glob(self.DatasetPath):                        #Read all files in the folder as a list and loops over each element
            count += 1
            gc.collect()
            if(count > 842):
                self.writeDatasetInHDD(volume)
                volume += 1
                count = 0
                self.Dataset = np.empty((0,self.Imagesize**2),float)
            Image = scipy.misc.imread(filename)                             #Read from the HDD
            if(Image.ndim == 3):                                            #If its a colour image
                if(Image.shape[2] == 3):                                    #Some files had bad dimensions
                    Image = self.convertToGreyscale(Image)                  #Convert to Greyscale if its a colour image
                    Image = scipy.misc.imresize(Image, self.ImageDim).flatten() #Resize the image to the desired dimension 
                    self.Dataset = np.vstack((self.Dataset, Image))         #Append the flattened image into the variable
            print self.Dataset.shape
        self.writeDatasetInHDD(volume)                                            #Write the Dataset in a cPickle file
        return None
        
    def writeDatasetInHDD(self, volume):
        """Write the dataset variable in HDD"""
        
        dic = {}
        dic['dataset'] = self.Dataset
        fileobject = open("faces"+str(volume),'w')
        cPickle.dump(dic,fileobject,protocol = 2)                           #Write in protocol 2 since its faster
        fileobject.close()
        return None
            
            
    def convertToGreyscale(self, Image):
        """Convert to greyscale if the image is colour image"""
        
        Image = np.dot ( Image, [0.299, 0.587, 0.144] )
        return Image


if __name__ == "__main__":
    t1 = time.time()
    Obj = vectorize()
    Obj.ProcessImages()
    print time.time()-t1