
import numpy as np
import matplotlib.pyplot as plt
import pylab
from quickhull2d import *
from Preprocess import KMAXOIDS



# function to plot / visualize Frank Wolfe examples
def plotExample(x, zc, Z, doDistPlot=True, nx=100, ny=100):
    fig = plt.figure()
    axs = fig.add_subplot(111, aspect='equal')

    # plot distance function
    if doDistPlot:
        xs, ys = np.meshgrid(np.linspace(-1,1,nx),np.linspace(-1,1,ny))
        X = np.vstack((xs.flatten(), ys.flatten()))
        F = np.sum((x-X)**2, axis=0).reshape(nx,ny)
        axs.imshow(F, origin='lower', extent=[-1,1,-1,1], cmap='BrBG')

    # plot data points
    H = qhull(Z.T)
    H = H.T
    axs.plot(Z[0,:], Z[1,:], 'ko', ms=6)
    axs.plot(H[0,:], H[1,:], 'k-', lw=2)
    axs.plot(x[0], x[1], 'rs', ms=10)
    axs.plot(zc[0], zc[1], 'ro', ms=10)

    plt.show()
    return None

def Kmaxoids( X, k):
    """Do k-Maxoids clustering and return maxoids"""

    Obj = KMAXOIDS.KMAXOIDS(maxoids=k)
    return Obj.doKmaxAndGet(X)



def plotExample2(x, zc, maxoids, filename, Z, doDistPlot=True, nx=100, ny=100):
    fig = plt.figure()
    axs = fig.add_subplot(111, aspect='equal')

    # plot distance function
    if doDistPlot:
        xs, ys = np.meshgrid(np.linspace(-1,1,nx),np.linspace(-1,1,ny))
        X = np.vstack((xs.flatten(), ys.flatten()))
        F = np.sum((x-X)**2, axis=0).reshape(nx,ny)
        pylab.imshow(F, origin='lower', extent=[-1,1,-1,1], cmap='Greys')

    # plot data points
    H = qhull(maxoids.T)
    H = H.T

    pylab.plot(Z[0,:], Z[1,:], 'ko', ms=6)
    pylab.plot(H[0,:], H[1,:], 'k-', lw=2)
    pylab.plot(x[0], x[1], 'gs', ms=10,label='Actual')
    pylab.plot(zc[0], zc[1], 'ro', ms=10,label='Approx')
    #for the line connecting actual and approximated point
    E = np.hstack((x,zc))
    pylab.plot(E[0,:], E[1,:], 'b-', lw=2,label='Error')
    pylab.legend(loc='lower right')
    #pylab.show()
    pylab.savefig(filename)

    return None


 # Frank Wolfe procedure to compute convexity constrained coefficients
def computeA(O, ZtZ, ZtX, A, tmax=1000):
    k, n = A.shape
    inds = np.arange(n)

    for t in range(tmax):
        gA = np.dot(ZtZ, A) - ZtX
        mininds = np.argmin(gA, axis=0)
        O[mininds,inds] = 1.
        a = 2./(t+2)
        A = A + a * (O - A)
        O[mininds,inds] = 0.

    return A





if __name__ == '__main__':
    #
    # example 1
    #

    # create a set Z of 50 random 2D points z_i (think of them as archetypes)
    n = 50
    H = np.random.randn(2,n) * 0.25

    #Find maxoids as Archetyes
    Archetypes = 7
    Z = Kmaxoids(H.T,Archetypes).T


    # create a single 2D query point x (store it in a matrix X)
    X = np.array([[-0.3], [0.7]])

    # helper matriox of all zeros
    O = np.zeros((Archetypes,1))

    # initial guess for coefficients
    Ainit = np.ones((Archetypes,1)); Ainit[0,:] = 1.

    # precompute Gram matrix X'X
    XtX = np.dot(X.T, X)

    # compute ingredients of gradient of E
    ZtZ = np.dot(Z.T, Z)
    ZtX = np.dot(Z.T, X)

    # finally compute coefficients
    A = computeA(O, ZtZ, ZtX, Ainit)
    plotExample2(X, np.dot(Z, A), Z, 'Outside.jpg',H)

    #Example of a point inside hull
    X = np.array([[0.3], [0.12]])

    # helper matriox of all zeros
    O = np.zeros((Archetypes,1))

    # initial guess for coefficients
    Ainit = np.ones((Archetypes,1)); Ainit[0,:] = 1.

    # precompute Gram matrix X'X
    XtX = np.dot(X.T, X)

    # compute ingredients of gradient of E
    ZtZ = np.dot(Z.T, Z)
    ZtX = np.dot(Z.T, X)

    # finally compute coefficients
    A = computeA(O, ZtZ, ZtX, Ainit)
    plotExample2(X, np.dot(Z, A), Z, 'Inside.jpg',H)





    #
    # example 2
    #

    # create a set Z of 3 archetypes z_i
    k = 3
    Z = np.array([[-1.5, 1.0,  0.0],
                  [ 1.0, 1.5, -1.0]])

    # create a set X of 25 data points
    n = 25
    X = np.random.randn(2,n) * 0.75

    # helper matriox of all zeros
    O = np.zeros((k,n))

    # initial guess for coefficients
    Ainit = np.ones((k,n)); Ainit /= k
    print Ainit
    # precompute Gram matrix X'X
    XtX = np.dot(X.T, X)

    # compute ingredients of gradient of E
    ZtZ = np.dot(Z.T, Z)
    ZtX = np.dot(Z.T, X)

    # finally compute coefficients
    A = computeA(O, ZtZ, ZtX, Ainit)

    print A
    #plotExample(X, np.dot(Z, A), Z, doDistPlot=False)
