import numpy as np
from Globals import *


class KMEANS():
    def __init__(self, means = 10):
        
        self.resultPath = PATH
        self.count = 0
        self.k = means
        self.N = np.ones(self.k)

    def train(self, X):
        
        if(X.ndim>1):
            for x in X:
                self.onlinekMeans(x)
        else:
            self.onlinekMeans(X)
            
        return None
    
    def onlinekMeans(self,x):
        
        if(self.count < self.k):                                        #Initialising the centroids until the number of clusters 
            if(self.count == 0):
                self.M = x
            else:
                self.M = np.vstack((self.M, x))
        else:

            i = np.argmin(np.sum((self.M - x)**2,axis=1))
            self.N[i] = self.N[i] + 1
            self.M[i] = self.M[i] + 1./self.N[i] * (x - self.M[i])
            
        self.count = self.count + 1
        
        return None
        
    def getCentroids(self):
        return self.M

    
    