'''
Created on Jul 26, 2015

@author: vignesh
'''
import numpy as np
from random import randint
from Globals import *
from math import floor
import time
from Preprocess import CIFAR10,FileManager,Kaggle2,KMAXOIDS,KMEANS,Faces
from Preprocess import StoreMeans as Sm
from cvxpy import *
import gc
from sklearn.metrics.pairwise import pairwise_distances
from sklearn.metrics import mean_squared_error
import scipy.sparse.linalg as sla


class Image_Analyser():


    def __init__(self):
        '''
        Input the Image_size, Patch_size, Classes as an integer
        and Dataset_path as string for path
        This class is used to all computation on the image
        '''
        
        self.Image_size = IMAGE_SIZE                                                                
        self.Patch_size = PATCH_SIZE
        self.Base_Patch_size = PATCH_SIZE
        self.critical_aspect_ratio = self.Patch_size / float(self.Image_size)
        self.num_of_rand_patches = NO_OF_RANDOM_PATCHES
        self.KMeans = K_MEANS
        self.colours = COLOURS
        self.patch_set = np.empty((0,self.colours*self.Patch_size*self.Patch_size),float)                #To collect the patches extracted
        self.col_wise_total = floor(self.Image_size/self.Patch_size)                        #No. of patches possible to extract in a column
        self.row_wise_total = floor(self.Image_size/self.Patch_size)                        #No. of patches possible to extract in a row
        self.total_possible_patches = int(self.col_wise_total * self.row_wise_total)        #Total No. of patches that can be extracted from the image
        self.resultPath = PATH
        self.KmeansObj = Sm.StoreMeans()
        self.datasetObj = DATASETOBJ
        self.fileObj = FileManager.FileManager()
        self.Visualize = VISUALIZE
        self.ClusterType = CLUSTER_TYPE
        self.Optimizer = OPT
        
        
    def createClusteringObject(self, size):
        """Create the Object for the Clustering Class which you specified in GLOBALS file"""
        
        if(self.ClusterType == "KMAXOIDS"):
            return KMAXOIDS.KMAXOIDS(self.KMeans[size])
        elif (self.ClusterType == "KMEANS"):
            return KMEANS.KMEANS(self.KMeans[size])

    
    def extractPatchesAtRandom(self,ClusterType = CLUSTER_TYPE):
        """Generates a set of Random patches from the given set of images"""
        
        t1 = time.time()
        path = str(self.resultPath)+"Patches/"
        self.fileObj.validateFilePath(path)
        self.ClusterType = ClusterType
        self.clutseringObj = self.createClusteringObject(self.Base_Patch_size)
        for outer_loop in xrange(NO_OF_LOOPS):
            gc.collect()
            for inner_loop in xrange(self.num_of_rand_patches):
                Image = self.datasetObj._chooseRandomImage()[0]                     #Just get the image at index 0 and not the class label since this is un-supervised Learning
                patch = self._computePatchAtRandom(Image)
                self.clutseringObj.train(patch)
                
                
            print "time to generate patches on round "+str(outer_loop)+" is :",time.time() - t1
                    
        return None

    
    def _computePatchAtRandom(self, Image):
        """Based on the Image and patch size, the function extracts a patch and flattens it before returning"""
        
        dim = Image.shape
        row = randint(0,dim[0]-self.Patch_size)                     #Extracting patch anywhere inside the image
        col = randint(0,dim[1]-self.Patch_size)
        patch = Image[row:(row+self.Patch_size),col:(col+self.Patch_size),:].flatten()
        return patch
    
    def computeCentroids(self):
        """Compute K means for original and whittened patches"""
        
        self.KmeansObj.computeKMeans(self.clutseringObj.getCentroids(), self.Patch_size)
        return None
    

    def FrankWolfeReconstruction(self, Z, X, PatchSize, tmax = 1000):
        """Perform Constrained Convex optimization using frank wolfe Algorithm for image reconstruction"""
        
        k = self.KMeans[PatchSize]
        n = X.shape[1]
        
        # helper matrix of all zeros
        O = np.zeros((k,n))
        
        # initial guess for coefficients
        A = np.ones((k,n)); A /= k

    
        # compute ingredients of gradient of E
        #ZtZ = np.dot(Z.T, Z)
        ZtX = np.dot(Z.T, X)
        
        k, n = A.shape
        inds = np.arange(n)
        for t in range(tmax):
            gA = np.dot(self.ZtZ, A) - ZtX
            mininds = np.argmin(gA, axis=0)
            O[mininds,inds] = 1.
            a = 2./(t+2)
            A = A + a * (O - A)
            O[mininds,inds] = 0.

        return A

    def reconstructByPseudoInverse(self,Z, Patches, PatchSize):
        """Reconstruct the image as linear combination of Archetypes"""
        A = np.dot(np.linalg.pinv(Z),Patches)
        return A
    def reconstructOptimize(self, Z, Patches, PatchSize):
        """Perform Constrained Convex optimization for image reconstruction"""

        
        N, D = Patches.shape
        coEffSet = np.empty((0,self.KMeans[PatchSize]),float)
        for i in xrange(N):
            X = Patches[i]
            
            # Construct the problem.
            a = Variable(self.KMeans[PatchSize])
            objective = Minimize(sum_squares(Z*a - X))
            constraints = [(a >= 0), (a <= 1), (sum(a) == 1)]
            prob = Problem(objective, constraints)
             
            # The optimal objective is returned by prob.solve().
            result = prob.solve()

            coEffSet = np.vstack((coEffSet,np.array(a.value).flatten()))
            
        
            
        return coEffSet.T
    
    
    def reconstructImage(self,OrgImage,previous_patch_size, newImage = 0):
        """Reconstruct a new image with nearest means in an optimized way"""
        
        #Reconstruction on Patches at lowest layer and on features at higher layers
        if(previous_patch_size == self.Base_Patch_size):
        
            PatchSize = self.Base_Patch_size
             
            #For cropping unwanted pixels
            dim = OrgImage.shape
            OrgImage = OrgImage[0:(0+(dim[0]/PatchSize)*PatchSize),0:(0+(dim[1]/PatchSize)*PatchSize),:]
            
            dim = OrgImage.shape
            patches = np.swapaxes(OrgImage.reshape(dim[0]/PatchSize,PatchSize,dim[1]/PatchSize,PatchSize,self.colours),1,2).reshape((dim[0]/PatchSize)*(dim[1]/PatchSize),PatchSize,PatchSize,self.colours)
            patches = patches.reshape((dim[0]/PatchSize)*(dim[1]/PatchSize),(PatchSize**2)*self.colours)
            
            #Reconstructing image with the closest means
            if(self.Optimizer == "FRANK_WOLFE"):
                X = self.FrankWolfeReconstruction(self.means[PatchSize].T, patches.T, PatchSize)    #Column stockhastic matrix
            elif(self.Optimizer == "CVXPY"):
                X = self.reconstructOptimize(self.means[PatchSize].T, patches, PatchSize)
            else:
                X = self.reconstructByPseudoInverse(self.means[PatchSize].T, patches.T, PatchSize)

            if (self.Visualize == True):
                newImage = np.dot(X.T,self.means[PatchSize])
                newImage = np.swapaxes(newImage.reshape((dim[0]/PatchSize)*(dim[1]/PatchSize),PatchSize,PatchSize,self.colours).reshape((dim[0]/PatchSize),(dim[1]/PatchSize),PatchSize,PatchSize,self.colours),1,2)
                newImage = newImage.reshape((dim[0]/PatchSize)*PatchSize,(dim[1]/PatchSize)*PatchSize,self.colours)

            
            WImage = np.swapaxes(X,1,0).reshape(dim[0]/PatchSize,dim[1]/PatchSize,self.KMeans[PatchSize])
        
        else:
            PatchSize = previous_patch_size
            #For cropping unwanted pixels
            dim = np.array([self.Image_size,self.Image_size])#Actual Image size, this is used to visulize the reconstructed image
            newDim = OrgImage.shape
            OrgImage = OrgImage[0:(0+(newDim[0]/2)*2),0:(0+(newDim[1]/2)*2),:]
            
            #For collecting the set of patches to do reconstruction for each patch
            newDim = OrgImage.shape
            
            patches = np.swapaxes(OrgImage.reshape(newDim[0]/2,2,newDim[1]/2,2,self.KMeans[PatchSize/2]),1,2).reshape((newDim[0]/2)*(newDim[1]/2),2,2,self.KMeans[PatchSize/2])
            patches = patches.reshape((newDim[0]/2)*(newDim[1]/2),(2*2)*self.KMeans[PatchSize/2])        

            #For computing pairwise patch reconstruction
            if(self.Optimizer == "FRANK_WOLFE"):
                X = self.FrankWolfeReconstruction(self.means[PatchSize].T, patches.T, PatchSize)        #Column stockhastic matrix
            elif(self.Optimizer == "CVXPY"):
                X = self.reconstructOptimize(self.means[PatchSize].T, patches, PatchSize)
            else:
                X = self.reconstructByPseudoInverse(self.means[PatchSize].T, patches.T, PatchSize)
            if (self.Visualize == True):
                newImage = np.dot(X.T,self.meanImages[PatchSize])
                newImage = np.swapaxes(newImage.reshape((dim[0]/PatchSize)*(dim[1]/PatchSize),PatchSize,PatchSize,self.colours).reshape((dim[0]/PatchSize),(dim[1]/PatchSize),PatchSize,PatchSize,self.colours),1,2)
                newImage = newImage.reshape((dim[0]/PatchSize)*PatchSize,(dim[1]/PatchSize)*PatchSize,self.colours)

            
            WImage = np.swapaxes(X,1,0).reshape(newDim[0]/2,newDim[1]/2,self.KMeans[PatchSize])


        return newImage, WImage
    
    
    def reconstructAndExtractPatch(self, patchSize, patchCount ,loops):
        """Reconstruct the image with means and then extract patches of next size"""
        
        self.Patch_size = patchSize
        previous_patch_size = patchSize / 2
        self.means = self.fileObj.readAllMeans(previous_patch_size)
        #Computing once for image reconstruction using Frank Wolfe Algorithm
        self.ZtZ = np.dot(self.means[previous_patch_size],self.means[previous_patch_size].T)
        if (previous_patch_size != self.Base_Patch_size):               #To check image reconstuction at higher levels
            self.meanImages = self.fileObj.readAllMeanImages(previous_patch_size)
        self.clutseringObj = self.createClusteringObject(self.Patch_size)
        t1 = time.time()
        for outer_loop in xrange(loops):
  
            print "Round :",outer_loop
            #Init the variable to collect intermediate results
            self.coeffSet = np.empty((0,((self.Image_size/previous_patch_size)**2)*self.KMeans[previous_patch_size]),float)
            if(previous_patch_size != self.Base_Patch_size):
                prev_CoeffSet = self.fileObj.readCoeffVector(previous_patch_size, outer_loop)
            for i in xrange(patchCount):
                print i
                gc.collect()
                
                #If this is at the base layer, we need to get the images from Dataset since no coeff were computed so far
                if(previous_patch_size == self.Base_Patch_size):
                    OrgImage = self.datasetObj._chooseRandomImage()[0]      #Just get the image at index 0 and not the class label since this is un-supervised Learning
                else:
                    OrgImage = prev_CoeffSet[i].reshape((self.Image_size/(previous_patch_size/2)),(self.Image_size/(previous_patch_size/2)),self.KMeans[previous_patch_size/2])
                
                newImage,WImage = self.reconstructImage(OrgImage,previous_patch_size)

                if (self.Visualize == True):
                    self.fileObj.saveImage("c1.jpg",self.fileObj.resizeImage(OrgImage,(32,32)))
                    self.fileObj.saveImage("c2.jpg",self.fileObj.resizeImage(newImage,(32,32)))
                    self.fileObj.saveImage("c3.jpg",self.fileObj.resizeImage(OrgImage-newImage,(32,32)))
                    assert 0,1
                if(previous_patch_size != self.Image_size):
                    #This layer is when the image patches are represented by co-eff
                    dim = WImage.shape

                    #Collect the feature vectors for storing them
                    self.coeffSet = np.vstack((self.coeffSet,WImage.flatten()))
                    for i in xrange ( dim[0] - 1 ) :                        #Extract patches from the images like a sliding window
                        for j in xrange ( dim[1] - 1 ) :
                            patch = WImage[i:(i+2),j:(j+2),:].flatten()
                            self.clutseringObj.train(patch)                 #Train the model with extracted patch in online fashion
                else:
                    #This last layer is just storing the entire image a set of co-eff
                    self.clutseringObj.train(WImage.flatten())
                        
            #Write the collected Feature Vectors in the HDD
            if(previous_patch_size != self.Image_size):
                self.fileObj.writeCoeffVector(self.coeffSet,patchSize,outer_loop)
              
            #For Kmeans on recontructed Patches
            print self.clutseringObj.getCentroids().shape
            self.KmeansObj.processMeans(self.clutseringObj.getCentroids(), self.resultPath, self.Patch_size)
            print  "Time taken for round ",outer_loop," is : ",time.time()-t1 
          
  
        return None
        
    def reconstructPatchwise(self,patchSize, patchCount, loops):
        """This method is for working on reconstruction of image with the closest mean image patch"""

        self.Patch_size = patchSize
        previous_patch_size = patchSize / 2
        self.means = self.fileObj.readAllMeans(previous_patch_size)
        self.datasetObj.count = -1#To start from the first image
        self.clutseringObj = self.createClusteringObject(self.Patch_size)
        t1 = time.time()
        count = 0
        for outer_loop in xrange(loops):

            print "Round :",outer_loop
            #Init the variable to collect intermediate results
            self.coeffSet = np.empty((0,(self.Image_size**2)*self.colours),float)
            if(previous_patch_size != self.Base_Patch_size):
                prev_CoeffSet = self.fileObj.readCoeffVector(previous_patch_size, outer_loop)
            for i in xrange(patchCount):
                #print "Round "+str(outer_loop)+" Image No. "+str(i)
                gc.collect()

                #If this is at the base layer, we need to get the images from Dataset since no coeff were computed so far
                if(previous_patch_size == self.Base_Patch_size):
                    OrgImage = self.datasetObj._chooseRandomImage()[0]      #Just get the image at index 0 and not the class label since this is un-supervised Learning
                else:
                    OrgImage = prev_CoeffSet[i].reshape(self.Image_size, self.Image_size, self.colours)

                newImage = self.reconstructByClosestMean(OrgImage,previous_patch_size)
                #self.datasetObj.input_dataset[count%59999] = newImage.flatten()
                print count
                count += 1
                if (self.Visualize == True):
                    self.fileObj.saveImage("c1.jpg",self.fileObj.resizeImage(OrgImage,(32,32)))
                    self.fileObj.saveImage("c2.jpg",self.fileObj.resizeImage(newImage,(32,32)))
                    self.fileObj.saveImage("c3.jpg",self.fileObj.resizeImage(OrgImage-newImage,(32,32)))
                    assert 0,1
                if(previous_patch_size != self.Image_size):
                    for i in xrange(int((self.Image_size-self.Patch_size)/float(previous_patch_size))+1):                #For extracting the patches like a sliding window
                        for j in xrange(int((self.Image_size-self.Patch_size)/float(previous_patch_size))+1):
                            row = i*previous_patch_size
                            col = j*previous_patch_size
                            patch = newImage[row:(row+self.Patch_size),col:(col+self.Patch_size),:]         #Extracting the patch from the image with new means
                            self.clutseringObj.train(patch.flatten())

                    #Collect the feature vectors for storing them
                    self.coeffSet = np.vstack((self.coeffSet,newImage.flatten()))

                else:
                    #This last layer is just storing the entire image a set of co-eff
                    self.clutseringObj.train(newImage.flatten())

            #Write the collected Feature Vectors in the HDD
            if(previous_patch_size != self.Image_size):
                self.fileObj.writeCoeffVector(self.coeffSet,patchSize,outer_loop)

            #For Kmeans on recontructed Patches
            print self.clutseringObj.getCentroids().shape
            self.KmeansObj.computeKMeans(self.clutseringObj.getCentroids(), self.Patch_size)
            print  "Time taken for round ",outer_loop," is : ",time.time()-t1


        return None

    def reconstructByClosestMean(self, OrgImage,previous_patch_size, newImage = 0):
        """Reconstruct the image patches with the closest Mean"""

        PatchSize = previous_patch_size
        #For cropping unwanted pixels
        dim = OrgImage.shape
        OrgImage = OrgImage[0:(0+(dim[0]/PatchSize)*PatchSize),0:(0+(dim[1]/PatchSize)*PatchSize),:]

        dim = OrgImage.shape
        patches = np.swapaxes(OrgImage.reshape(dim[0]/PatchSize,PatchSize,dim[1]/PatchSize,PatchSize,self.colours),1,2).reshape((dim[0]/PatchSize)*(dim[1]/PatchSize),PatchSize,PatchSize,self.colours)
        patches = patches.reshape((dim[0]/PatchSize)*(dim[1]/PatchSize),(PatchSize**2)*self.colours)

        #Reconstructing image with the closest means
        distMatrix = pairwise_distances(self.means[PatchSize],patches, metric='euclidean')                 #Returns the pairwise distance matrix
        newImage =  self.means[PatchSize][np.argmin(distMatrix,0)]                                         #Choose the corresponding smallest distance index
        newImage = np.swapaxes(newImage.reshape((self.Image_size/previous_patch_size)**2,previous_patch_size,previous_patch_size,self.colours).reshape(self.Image_size/previous_patch_size,self.Image_size/previous_patch_size,previous_patch_size,previous_patch_size,self.colours),1,2).reshape(self.Image_size,self.Image_size,self.colours)


        return newImage