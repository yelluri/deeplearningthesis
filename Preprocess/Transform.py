'''
Created on Feb 29, 2016

@author: vignesh
'''

import numpy as np
from Globals import *
from Preprocess import CIFAR10,FileManager,Kaggle2,KMAXOIDS,KMEANS
import cPickle
import scipy.misc
import gc
import time
from sklearn.metrics.pairwise import pairwise_distances
import types
import copy_reg
from multiprocessing import Pool
def _pickle_method(m):
    if m.im_self is None:
        return getattr, (m.im_class, m.im_func.func_name)
    else:
        return getattr, (m.im_self, m.im_func.func_name)

copy_reg.pickle(types.MethodType, _pickle_method)
class Transform():
    '''
    This Class is used to convert the set of images into set of convex combinations of archetypes
    The archetypes were found at patch size in a hierarchical fashion
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.fileObj = FileManager.FileManager()
        self.resultPath = PATH
        self.Base_Patch_size = PATCH_SIZE
        self.colours = COLOURS
        self.KMeans = K_MEANS
        self.datasetObj = DATASETOBJ
        self.Image_size = IMAGE_SIZE
        self.level = self.Image_size / (2*2*2)
        self.means = self.fileObj.readAllMeans(self.level)
        #self.meanImages = self.fileObj.readAllMeanImages(self.level)
        self.Total_classes = self.datasetObj.Total_classes
        self.Archetypes = self.means[self.level]
        self.Visualize = VISUALIZE
        self.VisualizePath = PATH+"Features/"
        self.fileObj.validateFilePath(self.VisualizePath)
        self.computeZtZ()                                                               #Compute once and use it multiple times, saves a lot of computation in FW algorithm


    def ReconstructWithClosestMean(self,OrgImage):
        """Reconstructs with the closest mean of a patch"""

        PatchSize = self.Base_Patch_size
        #For cropping unwanted pixels

        while(PatchSize <= self.level):
            dim = OrgImage.shape
            OrgImage = OrgImage[0:(0+(dim[0]/PatchSize)*PatchSize),0:(0+(dim[1]/PatchSize)*PatchSize),:]

            dim = OrgImage.shape
            patches = np.swapaxes(OrgImage.reshape(dim[0]/PatchSize,PatchSize,dim[1]/PatchSize,PatchSize,self.colours),1,2).reshape((dim[0]/PatchSize)*(dim[1]/PatchSize),PatchSize,PatchSize,self.colours)
            patches = patches.reshape((dim[0]/PatchSize)*(dim[1]/PatchSize),(PatchSize**2)*self.colours)

            #Reconstructing image with the closest means
            distMatrix = pairwise_distances(self.means[PatchSize],patches, metric='euclidean')                 #Returns the pairwise distance matrix
            newImage =  self.means[PatchSize][np.argmin(distMatrix,0)]                                         #Choose the corresponding smallest distance index
            newImage = np.swapaxes(newImage.reshape((self.Image_size/PatchSize)**2,PatchSize,PatchSize,self.colours).reshape(self.Image_size/PatchSize,self.Image_size/PatchSize,PatchSize,PatchSize,self.colours),1,2).reshape(self.Image_size,self.Image_size,self.colours)
            PatchSize = PatchSize * 2
            OrgImage = newImage

        return 0,newImage


    def convertToFeatureVector(self,Image):
        """Converts the given image into feature vectors with learned archetypes"""

        newImage = 0
        #print "Image"
        Image = np.array(Image).reshape(self.Image_size,self.Image_size,self.colours)
        PatchSize = self.Base_Patch_size

        #For cropping unwanted pixels
        dim = Image.shape
        OrgImage = Image[0:(0+(dim[0]/PatchSize)*PatchSize),0:(0+(dim[1]/PatchSize)*PatchSize),:]

        dim = OrgImage.shape
        patches = np.swapaxes(OrgImage.reshape(dim[0]/PatchSize,PatchSize,dim[1]/PatchSize,PatchSize,self.colours),1,2).reshape((dim[0]/PatchSize)*(dim[1]/PatchSize),PatchSize,PatchSize,self.colours)
        patches = patches.reshape((dim[0]/PatchSize)*(dim[1]/PatchSize),(PatchSize**2)*self.colours)

        #Reconstructing image with the closest means
        X = self.FrankWolfeReconstruction(self.means[PatchSize].T, patches.T, self.Base_Patch_size)    #Column stockhastic matrix

        if(self.Visualize == True):
            newImage = np.dot(self.means[PatchSize].T,X).T
            newImage = np.swapaxes(newImage.reshape((dim[0]/PatchSize)*(dim[1]/PatchSize),PatchSize,PatchSize,self.colours).reshape((dim[0]/PatchSize),(dim[1]/PatchSize),PatchSize,PatchSize,self.colours),1,2)
            newImage = newImage.reshape((dim[0]/PatchSize)*PatchSize,(dim[1]/PatchSize)*PatchSize,self.colours)

        WImage = np.swapaxes(X,1,0).reshape(dim[0]/PatchSize,dim[1]/PatchSize,self.KMeans[PatchSize])

        #For reconstruction in the furthur levels
        while(PatchSize < self.level):
            PatchSize = 2 * PatchSize

            if(PatchSize == self.Image_size*2):                                         #For final layer, no need to do pooling
                OrgImage = WImage
            else:
                #For cropping unwanted pixels
                newDim = WImage.shape
                OrgImage = WImage[0:(0+(newDim[0]/2)*2),0:(0+(newDim[1]/2)*2),:]

            #For collecting the set of patches to do reconstruction for each patch
            newDim = OrgImage.shape

            if(PatchSize == self.Image_size*2):                                         #For final layer, no need to do pooling
                patches = OrgImage.reshape(1,OrgImage.flatten().shape[0])
            else:
                patches = np.swapaxes(OrgImage.reshape(newDim[0]/2,2,newDim[1]/2,2,self.KMeans[PatchSize/2]),1,2).reshape((newDim[0]/2)*(newDim[1]/2),2,2,self.KMeans[PatchSize/2])
                patches = patches.reshape((newDim[0]/2)*(newDim[1]/2),(2*2)*self.KMeans[PatchSize/2])

            #For computing pairwise patch reconstruction
            X = self.FrankWolfeReconstruction(self.means[PatchSize].T, patches.T, PatchSize)        #Column stockhastic matrix

            if(self.Visualize == True):
                newImage = np.dot(self.meanImages[PatchSize].T,X).T
                newImage = np.swapaxes(newImage.reshape((dim[0]/PatchSize)*(dim[1]/PatchSize),PatchSize,PatchSize,self.colours).reshape((dim[0]/PatchSize),(dim[1]/PatchSize),PatchSize,PatchSize,self.colours),1,2)
                newImage = newImage.reshape((dim[0]/PatchSize)*PatchSize,(dim[1]/PatchSize)*PatchSize,self.colours)

            if(PatchSize == self.Image_size*2):                                         #For final layer, no need to do pooling
                WImage = X
            else:
                WImage = np.swapaxes(X,1,0).reshape(newDim[0]/2,newDim[1]/2,self.KMeans[PatchSize])
        return newImage, WImage.flatten()

    def computeZtZ(self):
        """Computes the Z.T * Z matrix once for Frank Wolfe Algorithm"""
        self.ZtZ = {}
        for i in self.means.keys():
            self.ZtZ[i] = np.dot(self.means[i],self.means[i].T)
        return None


    def FrankWolfeReconstruction(self, Z, X, PatchSize, tmax = 100):
        """Perform Constrained Convex optimization using frank wolfe Algorithm for image reconstruction"""

        k = self.KMeans[PatchSize]
        n = X.shape[1]

        # helper matrix of all zeros
        O = np.zeros((k,n))

        # initial guess for coefficients
        A = np.ones((k,n)); A /= k


        # compute ingredients of gradient of E
        #ZtZ = np.dot(Z.T, Z)
        ZtX = np.dot(Z.T, X)


        k, n = A.shape
        inds = np.arange(n)
        for t in range(tmax):
            gA = np.dot(self.ZtZ[PatchSize], A) - ZtX
            mininds = np.argmin(gA, axis=0)
            O[mininds,inds] = 1.
            a = 2./(t+2)
            A = A + a * (O - A)
            O[mininds,inds] = 0.
        return A




    def savebyCpickle(self,data,filename):
        """Write data into a Cpickle file in HDD"""

        fileobject = open(str(self.resultPath)+filename,'wb')
        cPickle.dump(data,fileobject,protocol = 2)                                   #Stores the patches in cPickle file
        fileobject.close()
        return None


    def readCpickleFile(self, filename):
        """Read a Cpickle from HDD"""

        fileobj = open(str(self.resultPath)+filename,'rb')
        data = cPickle.load(fileobj)
        fileobj.close()
        return data


    def createDataset(self):
        """This function converts the set of images into set of feature vectors
        These are nothing but the convex coefficients of the Archetypes"""
        FeatureLearning = False
        # pool = Pool()
        # t1 = time.time()
        # self.inputVec, self.output = self.datasetObj._readDataset()
        # print "Multi processing"
        # self.inputVec = pool.map(self.convertToFeatureVector,self.inputVec.tolist())
        # print " completed"
        # self.inputVec = np.array(self.inputVec)
        for j in xrange(10):
            neighbourhood = (self.Image_size / self.level)**2
            if(FeatureLearning):
                self.inputVec = np.empty((0,self.KMeans[self.level]*neighbourhood),dtype=float)
            else:
                self.inputVec = np.empty((0,self.Image_size**2),dtype=float)
            self.output = np.empty((0,1))
            for i in xrange(1000):
                print j,i
                gc.collect()
                #Image, Label = self.datasetObj._chooseRandomImage()
                Image, Label = self.datasetObj._chooseTestImage()
                if(FeatureLearning):
                    newImage,FeatureVec = self.convertToFeatureVector(Image)
                else:
                    newImage,FeatureVec = self.ReconstructWithClosestMean(Image)
                self.output = np.vstack((self.output, Label))
                self.inputVec = np.vstack((self.inputVec, FeatureVec.flatten()))
                #self.visualizeFeatures(Image,FeatureVec,i)
                if(self.Visualize == True):
                    self.fileObj.saveImage("c1.jpg",self.fileObj.resizeImage(Image,(500,500)))
                    self.fileObj.saveImage("c2.jpg",self.fileObj.resizeImage(newImage,(500,500)))
                    self.fileObj.saveImage("c3.jpg",self.fileObj.resizeImage(Image-newImage,(500,500)))
                    assert 0,1
            print self.output.shape,self.inputVec.shape
            train_set = {}
            train_set['input'] = self.inputVec
            train_set['output'] = self.output.flatten()
            self.savebyCpickle(train_set, "test"+str(j)+".p")
        return None

    def visualizeFeatures(self, Image, featureVec, count):
        """Save the feature vector as an image to see it"""

        self.fileObj.saveImage(self.VisualizePath+str(count)+"Image.jpg",self.fileObj.resizeImage(Image,(500,500)))
        self.fileObj.saveImage(self.VisualizePath+str(count)+"Feature.jpg",self.fileObj.resizeImage(featureVec.reshape(32,32),(500,500)))
        return None
if __name__ == "__main__":

    Obj = Transform()
    Obj.createDataset()