'''
Created on Feb 2, 2016

@author: vignesh
'''

import numpy as np
from random import randint
import cPickle
from Globals import *
from math import floor

class Faces(object):
    '''
    This class file is for the Dataset Object for Face Dataset
    It consists of ~8000 Images
    '''


    def __init__(self):
        self.Image_size = 128                                                                
        self.Patch_size = PATCH_SIZE
        self.colours = 1
        self.num_of_rand_patches = NO_OF_RANDOM_PATCHES
        self.KMeans = K_MEANS
        self.resultPath = PATH                       
        self.count = 0
        self.datasetpath = "/home/vignesh/Datasets/Face_Dataset/"
        self.inputDataset = self.readDataset()
        
        
    def readDataset(self):
        """Reads the cpickle file from the HDD"""
        
        fileobject = open(self.datasetpath+"faces"+str(randint(0,9)),'rb')      #Choose a partition among 10 such
        images = cPickle.load(fileobject)['dataset']
        fileobject.close()
          
        return images
    def _chooseRandomImage(self):
        """Chooses an Image at random from the dataset and returns Image,Class label"""
          
        if(self.count > 100):
            self.dictdata = self.readDataset()
            self.count = 0
        Image = self.inputDataset[randint(0,self.inputDataset.shape[0]-1)]
        self.count = self.count + 1
        
        return Image.reshape(self.Image_size,self.Image_size,self.colours),0        #Since there are no class labels, I return dummy class as second variable
        