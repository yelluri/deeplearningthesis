'''
Created on Jul 26, 2015

@author: vignesh
'''
import numpy as np
from random import randint
import scipy.misc
from scipy import linalg
from Globals import *
from math import floor
import scipy.cluster
from sklearn.decomposition import PCA
from sklearn import cluster
from Preprocess import Image_Analyser as IA

class Kaggle(IA.Image_Analyser):
    def __init__(self):
        super(Kaggle, self).__init__()
        
        
        self.Image_size = IMAGE_SIZE
        self.Patch_size = PATCH_SIZE
        self.critical_aspect_ratio = self.Patch_size / float(self.Image_size)
        self.num_of_rand_patches = NO_OF_RANDOM_PATCHES
        self.KMeans = K_MEANS
        self.patch_set = np.empty((0,3*self.Patch_size*self.Patch_size),int)                #To collect the patches extracted
        self.col_wise_total = floor(self.Image_size/self.Patch_size)                        #No. of patches possible to extract in a column
        self.row_wise_total = floor(self.Image_size/self.Patch_size)                        #No. of patches possible to extract in a row
        self.total_possible_patches = int(self.col_wise_total * self.row_wise_total)        #Total No. of patches that can be extracted from the image
        self.Kmeans_iterations = K_MEANS_ITERATIONS
        self.label = np.array(['cat','dog'])                                                     #Image names start with their class label only
        
        
    def _readImage(self, filename):
        """Reads the image from the given path and returns the image as a matrix"""
        
        Image = scipy.misc.imread(filename)
        Image = self._resizeImage(Image)
        
        return Image
    
    
    def _resizeImage(self, Image):
        """Resizes the given Image for having a normalized dimension image with same aspect ratio"""
        
        #newDim = np.array([self.Image_size,self.Image_size,3])                              #Specify the shape to which the image has to be resized
        dim = Image.shape
        newDim = [0,0]

        if (dim[0]>dim[1]):                                                                 #dim0 is higher
            
            ratio = dim[1] / float(dim[0])
            if (ratio < self.critical_aspect_ratio):
                
                ratio = self.critical_aspect_ratio
            newDim[0] = self.Image_size
            newDim[1] = int(self.Image_size * ratio)
            
        else:                                                                               #dim1 is higher
            
            ratio = dim[0] / float(dim[1])
            if (ratio < self.critical_aspect_ratio):
                ratio = self.critical_aspect_ratio
            newDim[0] = int(self.Image_size * ratio)
            newDim[1] = self.Image_size 
        
        resizedImage = scipy.misc.imresize(Image,newDim)
        
        return resizedImage
    
    
    def _chooseRandomImage(self):
        """Chooses an Image at random from the dataset"""
        
        flag = True
        while(flag):                                                                        # To check whether the image is highly rectangular 
                                                                                            #so that we can't get a patch
            fileName = "/home/vignesh/Datasets/Cats_Dogs/"+str(self.label[randint(0,1)])+"."+str(randint(0,10000))+".jpg"
            Image = self._readImage(fileName)
            if (self.critical_aspect_ratio < Image.shape[0]/float(Image.shape[1]) < 1.0/self.critical_aspect_ratio):
                flag = False

        return Image
        

    
    

    
    
    