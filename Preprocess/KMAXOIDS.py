import numpy as np
from Globals import *


class KMAXOIDS():
    def __init__(self, maxoids = 10):
        
        self.resultPath = PATH
        self.count = 0
        self.k = maxoids
        
    def train(self, X):
        
        if(X.ndim>1):
            for x in X:
                self.onlinekMaxoids(x)
        else:
            self.onlinekMaxoids(X)
            
        return None
    
    def onlinekMaxoids(self,x):
        
        if(self.count < self.k):                                        #Initialising the centroids until the number of clusters 
            if(self.count == 0):
                self.M = x
            else:
                self.M = np.vstack((self.M, x))
        else:
            
            D = np.sum((self.M - x)**2,axis=1)
            i = np.argmin(D)
            d = np.sum((self.M - self.M[i,:])**2,axis=1)

            D[i] = 0.
            d[i] = 0.
            px = np.sum(D)
            pm = np.sum(d)
            
            if px > pm:
                self.M[i,:] = x
        self.count = self.count + 1
        
        return None
        
    def getCentroids(self):
        return self.M

    def doKmaxAndGet(self, X):
        """Do compute maxoids and return them"""
        print "Computing kMaxoids..."
        for point in X:
            self.onlinekMaxoids(point)
        return self.M