import numpy as np
from Globals import *
import os
import cPickle
import scipy.misc

class FileManager():
    def __init__(self):
        
        self.resultPath = PATH
        self.Image_size = IMAGE_SIZE
        self.num_of_rand_patches = NO_OF_RANDOM_PATCHES
        self.colours = COLOURS
        self.basePatchSize = PATCH_SIZE
        
    def validateFilePath(self,path):
        """creates the directory if not exists"""
        
        if not os.path.exists(path): 
            os.makedirs(path)
            
        return path
    
    def writeCoeffVector(self, x, patchSize, partNo):
        """Write the coeffecient Vectors in HDD which were used for Reconstruction, this saves time in future when we again need to compute"""
        
        dic = {}
        dic['coeff'] = x
        path = str(self.resultPath)+"Coeff/"+str(patchSize)+"/"
        self.validateFilePath(path)
        fileobject = open(path+"set"+str(partNo)+".p",'wb')
        cPickle.dump(dic,fileobject,protocol = 2)                                   #Stores the patches in cPickle file
        fileobject.close()
        
        return None
    
    def readCoeffVector(self, patchSize, partNo):
        """Read the coeffVector stored after reconstruction from HDD"""
        
        fileobject = open(str(self.resultPath)+"Coeff/"+str(patchSize)+"/"+"set"+str(partNo)+".p",'rb')
        coEff = cPickle.load(fileobject)['coeff']
        fileobject.close()
        
        return coEff
        
            
    def readReconstructedPatchset(self, patchCount, loops, Patch_size, Patch_name):
        """Read the patches from HDD"""
        

        patch_set = np.empty((0,self.colours*Patch_size*Patch_size),float)
        for outer_loop in xrange(loops):
            fileobject = open(str(self.resultPath)+"Patches/patch_set"+str(outer_loop)+str(Patch_size)+str(self.Image_size)+str(patchCount)+".p",'rb')
            dic = cPickle.load(fileobject)
            patch_set = np.vstack((patch_set,dic[Patch_name]))

            fileobject.close()
            print "Reading block No:",outer_loop
            
            
        return patch_set
    
    def writeReconstructedPatchSet(self, patch_set, patch_set_means, patch_set_w_means, patchCount, outer_loop, Patch_size): 
        """Writes the patches in HDD"""
        
        dic = {}
        dic['patch_set'] = patch_set
        dic['patch_set_means'] = patch_set_means
        dic['patch_set_w_means'] = patch_set_w_means
        path = str(self.resultPath)+"Patches/"
        self.validateFilePath(path)
        fileobject = open(path+"patch_set"+str(outer_loop)+str(Patch_size)+str(self.Image_size)+str(patchCount)+".p",'wb')
        cPickle.dump(dic,fileobject,protocol = 2)                                   #Stores the patches in cPickle file
        fileobject.close()
        
        return None
    
    
    
    def readRandomPatches(self, Patch_size):
        """Read from HDD, the saved patches"""
        
        temp_set = np.empty((0,self.colours*Patch_size*Patch_size),int)
        for i in xrange(NO_OF_LOOPS):
            fileobject = open(str(self.resultPath)+"Patches/patch_set"+str(i)+str(Patch_size)+str(self.Image_size)+str(self.num_of_rand_patches)+".p",'rb')
            dic = cPickle.load(fileobject)
            temp_set = np.vstack((temp_set,dic['patch_set']))
            fileobject.close()
            print "Reading Block No:",i
        patch_set = temp_set
        print "shape of Data loaded is : ",patch_set.shape
                
        return patch_set
    
    
    
    def writeRandomPatches(self, patch_set, outer_loop, path, Patch_size): 
        """Writes the patches in HDD"""
        
        dic = {}
        dic['patch_set'] = patch_set
        self.validateFilePath(path)
        fileobject = open(path+"patch_set"+str(outer_loop)+str(Patch_size)+str(self.Image_size)+str(self.num_of_rand_patches)+".p",'wb')
        cPickle.dump(dic,fileobject,protocol = 2)                                   #Stores the patches in cPickle file
        fileobject.close()
        
        return None
    
    
    def writeBasisVectorsForWhitten(self, meanPatch, vec, L, Patch_size):
        """To write the basis to whitten data in future"""
        
        dic = {}                                                    #The Eigen Vectors, Eigen Val scaled to 1 and mean are stored in dictionary                  
        dic['vec'] = vec
        dic['mean'] = meanPatch
        dic['L'] = L                                                #L is the eigenvalues scaled to 1
        path = str(self.resultPath)+"Clusters/"
        self.validateFilePath(path)
        fileobject = open(path+"basis_"+str(Patch_size)+".p",'wb')
        cPickle.dump(dic,fileobject,protocol = 2)                                   #Stores the basis vectors in cPickle file
        fileobject.close()
        
        return None
    
    def readBasisVectorsForWhitten(self, Patch_size):
        """Read the basis vectors for whittening from HDD"""
        
        fileObj = open(str(self.resultPath)+"Clusters/basis_"+str(Patch_size)+".p",'rb')
        dic = cPickle.load(fileObj)
        meanPatch = dic['mean']                                #Storing the basis vectors as a class variable
        vec = dic['vec']
        L = dic['L']
        fileObj.close()
        
        return meanPatch, vec, L
    
    
    def writeMeans(self, centroids, resultPath, Patch_size):
        """Store the means computed in the HDD"""
        
        path = str(resultPath)+"Clusters/"
        self.validateFilePath(path)
        dic = {}
        dic['means'] = centroids
        fileobject = open(path+"means"+str(Patch_size)+".p",'wb')
        cPickle.dump(dic,fileobject,protocol = 2)                                   #Stores the mean vectors in cPickle file
        fileobject.close()
        
        return None
    
    def writeMeanImages(self, centroids, Patch_size):
        """Write the means converted from feature vectors in the HDD"""
        
        path = str(self.resultPath)+"Clusters/"
        self.validateFilePath(path)
        dic = {}
        dic['means'] = centroids
        fileobject = open(path+"ImageMeans"+str(Patch_size)+".p",'wb')
        cPickle.dump(dic,fileobject,protocol = 2)                                   #Stores the mean vectors in cPickle file
        fileobject.close()
        
        return None
    
    def readMeanImages(self, Patch_size):
        """Read the means converted from feature vectors from HDD"""
        
        fileObj = open(str(self.resultPath)+"Clusters/ImageMeans"+str(Patch_size)+".p",'rb')
        dic = cPickle.load(fileObj)
        means = dic['means']                                
        fileObj.close()
        
        return means
    
    def readMeans(self, Patch_size):
        """Load the means stored in HDD"""
        
        fileObj = open(str(self.resultPath)+"Clusters/means"+str(Patch_size)+".p",'rb')
        dic = cPickle.load(fileObj)
        means = dic['means']                                
        fileObj.close()
        
        
        return means
    
    def readAllMeanImages(self, Patch_size):
        """Loads all the mean Iamges from the base to current patch size"""
        
        currentPatchSize = self.basePatchSize * 2
        dic = {}
        while(currentPatchSize <= Patch_size):
            dic[currentPatchSize] = self.readMeanImages(currentPatchSize)
            currentPatchSize = 2 * currentPatchSize
            
        return dic
        
    
    def readAllMeans(self, Patch_size):
        """Loads all the means from the base to current patch size"""
        
        currentPatchSize = self.basePatchSize
        dic = {}
        while(currentPatchSize <= Patch_size):
            dic[currentPatchSize] = self.readMeans(currentPatchSize)
            currentPatchSize = 2 * currentPatchSize
            
        return dic
    
    def saveImage(self, filename, Image):
        """Reshape for properly saving the array as an image for grey images"""
        if (self.colours==3):
            scipy.misc.imsave(filename,Image)
        else:
            scipy.misc.imsave(filename,Image.reshape(Image.shape[:2]))
            
        return None
    
    def resizeImage(self, Image, dim):
        """Resize grey images properly"""
        if (self.colours==3):
            x = scipy.misc.imresize(Image,dim)
        else:
            x = scipy.misc.imresize(Image.reshape(Image.shape[:2]),dim)
            
        return x
        