import numpy as np
from random import randint
import cPickle
from Globals import *
from math import floor
from sklearn.decomposition import PCA
import gc
import glob
import scipy.misc
from sklearn.preprocessing import StandardScaler

class CBLC():
    def __init__(self):

        self.Image_size = 32                                                                #CIFAR Images has Images of size 32X32 only
        self.Patch_size = PATCH_SIZE
        self.num_of_rand_patches = NO_OF_RANDOM_PATCHES
        self.colours = COLOURS
        self.Total_classes = 2                                                          #CBLC Datset has faces and non faces
        self.patch_set = np.empty((0,self.colours*self.Patch_size*self.Patch_size),int)                #To collect the patches extracted
        self.resultPath = PATH
        self.Whitten = WHITTEN
        self.count = -1
        self.datasetpath = "/home/vignesh/Datasets/CBLC/"
        self.ImageDim = np.array([self.Image_size,self.Image_size])
        self.input_dataset,self.output_labels = self._readDataset()                         #Reads the data from the binary files
        #self.mergeTrainTest()


    def _chooseRandomImage(self):
        """Gets a Random Image from the dataset"""

        self.count += 1
        temp = self.count % 6976
        #temp = 15
        return self.input_dataset[temp][:].reshape(self.Image_size,self.Image_size,1), self.output_labels[temp]

    def _chooseTestImage(self,whitten=False):
        """Choose an image from the test Dataset"""

        if(self.count == -1):
            #Invoking this function for the first time, then we will read the test dataset and store it in a variable
            self._readTestSet()

        self.count += 1
        temp = self.count % self.testInput.shape[0]
        return self.testInput[temp][:].reshape(self.Image_size,self.Image_size,1),self.testOutput[temp]

    def _readDataset(self):
        """Reads the training data set form cPickle file"""
        if(self.Whitten == True):
            fileObject = open(self.datasetpath+str('wTrain.p'), 'rb')
        else:
            fileObject = open(self.datasetpath+str('train.p'), 'rb')
        images = cPickle.load(fileObject)
        fileObject.close()

        return images['input'],images['output']


    def _readTestSet(self):
        """Reads the test data set form cPickle file"""
        if(self.Whitten == True):
            fileObject = open(self.datasetpath+str('wTest.p'), 'rb')
        else:
            fileObject = open(self.datasetpath+str('test.p'), 'rb')
        images = cPickle.load(fileObject)
        fileObject.close()
        self.testInput,self.testOutput = images['input'],images['output']
        return self.testInput,self.testOutput


    def mergeTrainTest(self):
        """Do feature learning both on test and train data"""
        self._readTestSet()
        self.input_dataset = np.vstack((self.input_dataset,self.testInput))
        self.output_labels = np.append(self.output_labels,self.testOutput)
        del(self.testOutput)
        del(self.testInput)
        gc.collect()
        return None

    def readImages(self):
        """Reads the images and preprocess them"""

        trainFace = self.processImages(self.datasetpath+str('train/face/*.pgm'))
        print 'Read trainface'
        trainNonFace = self.processImages(self.datasetpath+str('train/non-face/*.pgm'))
        print 'read train n face'


        #Class Labels
        trainFaceLabels = np.ones(trainFace.shape[0])*(1.0)
        trainNonFaceLabels = np.ones(trainNonFace.shape[0])*(-1.0)

        #print trainFace.shape,trainNonFace.shape,testFace.shape,testNonFace.shape

        #Init the np variables
        trainInput = np.empty((0,self.Image_size**2),float)


        #Append the processed datga for training and test input images
        trainInput = np.vstack((trainInput,trainFace))
        trainInput = np.vstack((trainInput,trainNonFace))

        #print trainInput.shape,testInput.shape

        #Append the processed data for training and test output labels
        trainOutput = np.append(trainFaceLabels,trainNonFaceLabels)


        #Shuffle the datasets
        shuffler = np.arange(trainInput.shape[0])
        np.random.shuffle(shuffler)
        trainInput = trainInput[shuffler]
        trainOutput = trainOutput[shuffler]

        print 'shuffled'


        train = {}
        train['input'] = trainInput
        train['output'] = trainOutput



        self.writeDatasetInHDD('train.p',train)
        assert 0,1
        del (train)
        del (trainInput)
        del(trainNonFace)
        del (trainFace)

        #For test
        testFace = self.processImages(self.datasetpath+str('test/face/*.pgm'))
        print 'read test face'
        testNonFace = self.processImages(self.datasetpath+str('test/non-face/*.pgm'))
        print 'read test n face'

        testFaceLabels = np.ones(testFace.shape[0])*(1.0)
        testNonFaceLabels = np.ones(testNonFace.shape[0])*(-1.0)

        testInput = np.empty((0,self.Image_size**2),float)
        testInput = np.vstack((testInput,testFace))
        testInput = np.vstack((testInput,testNonFace))

        testOutput = np.append(testFaceLabels,testNonFaceLabels)

        shuffler = np.arange(testInput.shape[0])
        np.random.shuffle(shuffler)
        testInput = testInput[shuffler]
        testOutput = testOutput[shuffler]


        test = {}
        test['input'] = testInput
        test['output'] = testOutput

        self.writeDatasetInHDD('test.p',test)

        print 'done'
        return None


    def writeDatasetInHDD(self, filename,x):
        """Write the dataset variable in HDD"""

        fileobject = open(filename,'w')
        cPickle.dump(x,fileobject,protocol = 2)                           #Write in protocol 2 since its faster
        fileobject.close()
        return None

    def processImages(self,path):
        """Resize, vectorize and return as a matrix"""
        Dataset = np.empty((0,self.Image_size**2),float)
        for filename in glob.glob(path):                        #Read all files in the folder as a list and loops over each element
            Image = scipy.misc.imread(filename)
            Image = scipy.misc.imresize(Image, self.ImageDim).flatten() #Resize the image to the desired dimension
            Dataset = np.vstack((Dataset,Image))
        return Dataset


    def whittenData(self):
        """Whitten the dataset to see normalized images in the learning"""
        Scaler = StandardScaler()
        self.input_dataset = Scaler.fit_transform(self.input_dataset)
        self.testInput = Scaler.transform(self.testInput)
        train = {}
        train['input'] = self.input_dataset
        train['output'] = self.output_labels
        test = {}
        test['input'] = self.testInput
        test['output'] = self.testOutput
        self.writeDatasetInHDD('wTrain.p',train)
        self.writeDatasetInHDD('wTest.p',test)
        del (train)
        del (test)
        return None




# if __name__ == "__main__":
#
#     Obj = CBLC()
#     Obj._readTestSet()
#     Obj.whittenData()
#     print 'Done'


