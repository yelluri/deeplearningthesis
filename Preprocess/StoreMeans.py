import numpy as np
from Globals import *
import scipy.misc
from Preprocess import CIFAR10,FileManager
import cPickle
import new

class StoreMeans():
    def __init__(self):
        
        self.Kmeans = K_MEANS
        self.fileObj = FileManager.FileManager()
        self.colours = COLOURS
        self.Image_Size = IMAGE_SIZE
        self.basePatchSize = PATCH_SIZE
        self.resultPath = PATH


    
    def computeKMeans(self, centroids, Patch_size):
        """Computes K means Clustering and returns the K centroids"""

        path = str(self.resultPath)+"Clusters/"
        self.fileObj.validateFilePath(path)
        dic = {}
        dic['means'] = centroids
        fileobject = open(path+"means"+str(Patch_size)+".p",'wb')
        cPickle.dump(dic,fileobject)                                   #Stores the mean vectors in cPickle file
        fileobject.close()
        path = str(self.resultPath)+'ClusterImages/Clusters'+str(Patch_size)+'/'
        self.fileObj.validateFilePath(path)
        for i in xrange(centroids.shape[0]):                                                           #Save these means as images to visualize (also enlarge for better visualiztion)
            self.fileObj.saveImage(path+'patch '+str(Patch_size)+'.'+str(i)+'.jpg', self.fileObj.resizeImage(centroids[i].reshape(Patch_size,Patch_size,self.colours),(50,50)))
            
        return None
    
    
    def computeWhittenedKMeans(self, centroids, resultPath, Patch_size):
        """Computes K means Clustering and returns the K centroids for whittened data"""

        path = str(resultPath)+"Clusters/"
        self.fileObj.validateFilePath(path)
        dic = {}
        dic['W_means'] = centroids
        fileobject = open(path+"W_means"+str(Patch_size)+".p",'wb')
        cPickle.dump(dic,fileobject)                                   #Stores the mean vectors in cPickle file
        fileobject.close()
        path = str(resultPath)+'ClusterImages/Clusters'+str(Patch_size)+'/'
        self.fileObj.validateFilePath(path)
        for i in xrange(centroids.shape[0]):
            scipy.misc.imsave(path+'w_patch '+str(Patch_size)+'.'+str(i)+'.jpg', scipy.misc.imresize(centroids[i].reshape(Patch_size,Patch_size,self.colours),(50,50)))

        
        return None
    
    
    
    
    def processMeans(self, centroids, resultPath, Patch_size):
        """"Store the means and visualize the means as images recursively"""
        
        self.fileObj.writeMeans(centroids, resultPath, Patch_size)
        self.visualizeMeans ( centroids, resultPath, Patch_size )
        
        return None
    
    def converttoImages(self, centroids, Patch_size):
        """Store the means as images"""
        path = str(self.resultPath)+'ClusterImages/Clusters'+str(Patch_size)+'/'
        self.fileObj.validateFilePath(path)
        for i in xrange(centroids.shape[0]):
            if (self.Image_Size * 2 > Patch_size):
                #This processing is only for all layers which works on image patches and not on the final image as a whole
                self.fileObj.saveImage(path+'patch '+str(Patch_size)+'.'+str(i)+'.jpg', self.fileObj.resizeImage(centroids[i].reshape(Patch_size,Patch_size,self.colours),(50,50)))
            else:
                #This case is for visualizing the last layer weights or the weights of the entire image size
                self.fileObj.saveImage(path+'patch '+str(Patch_size)+'.'+str(i)+'.jpg', self.fileObj.resizeImage(centroids[i].reshape(Patch_size/2,Patch_size/2,self.colours),(50,50)))

        return None
            
    
    def visualizeMeans(self, centroids, resultPath, Patch_size):
        """Compute the means recursively to the deep levels to visualize"""

        previous_patch_size = Patch_size / 2
        if(previous_patch_size == self.basePatchSize):
            oldMeans = self.fileObj.readMeans(previous_patch_size)
        else:
            oldMeans = self.fileObj.readMeanImages(previous_patch_size)
        if (self.Image_Size * 2 > Patch_size):
            #This processing is only for all layers which works on image patches and not on the final image as a whole
            self.meanSet = np.empty((0,(Patch_size**2)*self.colours),float)
            for i in xrange(centroids.shape[0]):
                centroid = centroids[i][:].reshape(4,self.Kmeans[previous_patch_size]).T           #Column stockhastic matrix
                newImage = np.dot(oldMeans.T,centroid).T
                newImage = np.swapaxes(newImage.reshape((2)*(2),previous_patch_size,previous_patch_size,self.colours).reshape((2),(2),previous_patch_size,previous_patch_size,self.colours),1,2)
                newImage = newImage.reshape((2)*previous_patch_size,(2)*previous_patch_size,self.colours)
                self.meanSet = np.vstack((self.meanSet,newImage.flatten()))

        else:
            #This case is for visualizing the last layer weights or the weights of the entire image size
            self.meanSet = np.empty((0,((Patch_size/2)**2)*self.colours),float)
            for i in xrange(centroids.shape[0]):
                centroid = centroids[i][:].reshape(self.Kmeans[previous_patch_size],1)
                newImage = np.dot(oldMeans.T, centroid)
                self.meanSet = np.vstack((self.meanSet,newImage.flatten()))

        self.converttoImages(self.meanSet, Patch_size)
        self.fileObj.writeMeanImages(self.meanSet, Patch_size)
            
        return None
        