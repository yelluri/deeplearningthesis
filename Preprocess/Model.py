'''
Created on Jan 11, 2016

@author: vignesh
'''

import numpy as np
from random import randint
from Globals import *
from Preprocess import CIFAR10,FileManager,Kaggle2,KMAXOIDS,KMEANS
import cPickle
import gc
import sklearn.metrics.pairwise as pair_dist

from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report

class Model():
    '''
    This Class is used to label the Archetypes or Representatives found and then use these labelled
    Archetypes to classify a new unseen Image
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.fileObj = FileManager.FileManager()
        self.resultPath = PATH
        self.Base_Patch_size = PATCH_SIZE
        self.colours = COLOURS
        self.KMeans = K_MEANS
        self.datasetObj = DATASETOBJ
        self.Image_size = IMAGE_SIZE
        self.means = self.fileObj.readAllMeans(self.Image_size)
        self.meanImages = self.fileObj.readAllMeanImages(self.Image_size)
        self.Total_classes = self.datasetObj.Total_classes
        self.Archetypes = self.means[self.Image_size]
        self.PerclassLimit = 1000                                                  #this many number of images are collected per class to assign labels for learned Archetypes                       
       
    
    
    def convertToFeatureVector(self,Image):
        """Converts the given image into feature vectors with learned archetypes"""
        PatchSize = self.Base_Patch_size
         
        #For cropping unwanted pixels
        dim = Image.shape
        OrgImage = Image[0:(0+(dim[0]/PatchSize)*PatchSize),0:(0+(dim[1]/PatchSize)*PatchSize),:]
        
        dim = OrgImage.shape
        patches = np.swapaxes(OrgImage.reshape(dim[0]/PatchSize,PatchSize,dim[1]/PatchSize,PatchSize,self.colours),1,2).reshape((dim[0]/PatchSize)*(dim[1]/PatchSize),PatchSize,PatchSize,self.colours)
        patches = patches.reshape((dim[0]/PatchSize)*(dim[1]/PatchSize),(PatchSize**2)*self.colours)
        
        #Reconstructing image with the closest means       
        X = self.FrankWolfeReconstruction(self.means[PatchSize].T, patches.T, self.Base_Patch_size)    #Column stockhastic matrix

        newImage = np.dot(self.means[PatchSize].T,X).T

        newImage = np.swapaxes(newImage.reshape((dim[0]/PatchSize)*(dim[1]/PatchSize),PatchSize,PatchSize,self.colours).reshape((dim[0]/PatchSize),(dim[1]/PatchSize),PatchSize,PatchSize,self.colours),1,2)
        newImage = newImage.reshape((dim[0]/PatchSize)*PatchSize,(dim[1]/PatchSize)*PatchSize,self.colours)
        
        WImage = np.swapaxes(X,1,0).reshape(dim[0]/PatchSize,dim[1]/PatchSize,self.KMeans[PatchSize])
        
        #For reconstruction in the furthur levels
        while(PatchSize < self.Image_size/2 ):
            PatchSize = 2 * PatchSize
            #For cropping unwanted pixels
            newDim = WImage.shape
            OrgImage = WImage[0:(0+(newDim[0]/2)*2),0:(0+(newDim[1]/2)*2),:]
            
            #For collecting the set of patches to do reconstruction for each patch
            newDim = OrgImage.shape
            
            patches = np.swapaxes(OrgImage.reshape(newDim[0]/2,2,newDim[1]/2,2,self.KMeans[PatchSize/2]),1,2).reshape((newDim[0]/2)*(newDim[1]/2),2,2,self.KMeans[PatchSize/2])
            patches = patches.reshape((newDim[0]/2)*(newDim[1]/2),(2*2)*self.KMeans[PatchSize/2])        

            #For computing pairwise patch reconstruction
            X = self.FrankWolfeReconstruction(self.means[PatchSize].T, patches.T, PatchSize)        #Column stockhastic matrix
    
            
            newImage = np.dot(self.meanImages[PatchSize].T,X).T
            newImage = np.swapaxes(newImage.reshape((dim[0]/PatchSize)*(dim[1]/PatchSize),PatchSize,PatchSize,self.colours).reshape((dim[0]/PatchSize),(dim[1]/PatchSize),PatchSize,PatchSize,self.colours),1,2)
            newImage = newImage.reshape((dim[0]/PatchSize)*PatchSize,(dim[1]/PatchSize)*PatchSize,self.colours)

            
            WImage = np.swapaxes(X,1,0).reshape(newDim[0]/2,newDim[1]/2,self.KMeans[PatchSize])
        return newImage,WImage
    
    
    def FrankWolfeReconstruction(self, Z, X, PatchSize, tmax = 100):
        """Perform Constrained Convex optimization using frank wolfe Algorithm for image reconstruction"""
        
        k = self.KMeans[PatchSize]
        n = X.shape[1]
        
        # helper matrix of all zeros
        O = np.zeros((k,n))
        
        # initial guess for coefficients
        A = np.ones((k,n)); A /= k

    
        # compute ingredients of gradient of E
        ZtZ = np.dot(Z.T, Z)
        ZtX = np.dot(Z.T, X)
        
        k, n = A.shape
        inds = np.arange(n)
        for t in range(tmax):
            gA = np.dot(ZtZ, A) - ZtX
            mininds = np.argmin(gA, axis=0)
            O[mininds,inds] = 1.
            a = 2./(t+2)
            A = A + a * (O - A)
            O[mininds,inds] = 0.
        return A
           
    def computeFeatureImages(self):
        """Compute the images as feature vectors which will then be used to label the Archetypes
        Here we would compute for n (say 1000) images for each class."""
        
        learnMore = True
        count = {}
        for i in xrange(self.Total_classes):
            #Assuming 0 is also a class label
            count[i] = 0
        print "Computing Feature Images"
        PerClassImage = {}
        temp = 0
        while(learnMore):
            """This collects 1000 images per each class"""
            
            Image, label = self.datasetObj._chooseRandomImage()
            if(count[label] < self.PerclassLimit):
                #To ensure no more images are needed for a particular class if we have enough
                temp += 1
                print temp
                #self.fileObj.saveImage("c1.jpg",self.fileObj.resizeImage(Image,(500,500)))
                visual, Image = self.convertToFeatureVector(Image)
#                 self.fileObj.saveImage("c2.jpg",self.fileObj.resizeImage(visual,(500,500)))
#                 assert 0,1
                if(label in PerClassImage.keys()):
                    PerClassImage[label] = np.vstack((PerClassImage[label], Image.flatten()))
                else:
                    PerClassImage[label] = Image.flatten()
                count[label] += 1

            #To ensure stop of collection when all class images are collected upto a certain limit
            total = 0
            for i in count.keys():
                total += count[i]
            if (total >= self.Total_classes * self.PerclassLimit):
                learnMore = False
        #print count    
        return PerClassImage
            
                
                        
    def savebyCpickle(self,data,filename):
        """Write data into a Cpickle file in HDD"""
        
        fileobject = open(str(self.resultPath)+filename,'wb')
        cPickle.dump(data,fileobject,protocol = 2)                                   #Stores the patches in cPickle file
        fileobject.close()
        return None
                        
                        
    def readCpickleFile(self, filename):
        """Read a Cpickle from HDD"""
        
        fileobj = open(str(self.resultPath)+filename,'rb')
        data = cPickle.load(fileobj)
        fileobj.close()
        return data
                            
    def train(self):
        """Label the Archetypes with the closest Image
        We have the training data which is the images converted in to feature vectors
        and we have the archetypes"""
        
        trainData = self.readCpickleFile("transfromed.p")
        temp_labels = np.empty((0,self.Archetypes.shape[0]),float)
        for i in trainData.keys():
            distMatrix = pair_dist.pairwise_distances(self.Archetypes, trainData[i])
            temp_labels = np.vstack((temp_labels,np.min(distMatrix,axis=1)))
        temp_labels = np.argmin(temp_labels,axis=0)
#         self.classWiseArchetypes = {}
#         for i in trainData.keys():
#             #For stroing the archetypes class wise
#             min_inds = np.where(temp_labels == i)
#             min_inds = np.array(min_inds[0])
#             self.classWiseArchetypes[i] = self.Archetypes[min_inds]
        dic = {}
        dic["label"] = temp_labels
        self.savebyCpickle(dic, "labels.p")
        #self.savebyCpickle(self.classWiseArchetypes, "labels.p")
        return None
        
    def predict(self, X = None,Y = None,shape = None):
        """This method is used to predict the target class of the image
        images are flatten and stored as (NXD) and Y are labels if known
        You can provide the shape for reshaping an image which was sent as flat"""
        
        if(shape == None):
            shape = (self.Image_size,self.Image_size,self.colours)
        self.transfomedImages = np.empty((0,self.Archetypes.shape[1]),float)
        self.classWiseArchetypes = self.readCpickleFile("labels.p")["label"]

        #Transform the raw images as feature vectors
        if(X.ndim == 1):
            visual, Image = self.convertToFeatureVector(X.reshape(shape))
            self.transfomedImages = np.vstack((self.transfomedImages,Image.flatten()))
        else:
            for Image in X:
                visual, Image = self.convertToFeatureVector(Image.reshape(shape))
                self.transfomedImages = np.vstack((self.transfomedImages,Image.flatten()))
                
        #Predcit Class wise similarity by distance
#         predictedLabels = np.empty((0,self.transfomedImages.shape[0]),float)
#         for i in self.classWiseArchetypes.keys():
#             distMatrix = pair_dist.pairwise_distances(self.transfomedImages, self.classWiseArchetypes[i])
#             predictedLabels = np.vstack((predictedLabels,np.sum(distMatrix,axis = 1)))
#             print float(self.classWiseArchetypes[i].shape[0])
# 
#         predictedLabels = np.argmin(predictedLabels, axis = 0)
#         print predictedLabels,Y

        #train using any machine learning algorithms
        predcitedLabels = self.knn(k=1,trainX = self.Archetypes,trainY = self.classWiseArchetypes, testX = self.transfomedImages)
        print classification_report(Y,predcitedLabels)
        return None
    
    def knn(self,k,trainX,trainY,testX):
        """Train and predict with Knn classifier"""
        
        KnnObj = KNeighborsClassifier(n_neighbors=k,n_jobs = -1)
        KnnObj.fit(trainX, trainY)
        return KnnObj.predict(testX)
        
     
    def test(self):
        X = []
        Y = []
        for i in xrange(1000):
            Image, label = self.datasetObj._chooseRandomImage()
            X.append(Image.flatten())
            Y.append(label)
        self.predict(np.array(X), np.array(Y))
        
if __name__ == "__main__":
    Obj = Model()
    tranformedImages = Obj.computeFeatureImages()
    Obj.savebyCpickle(tranformedImages,"transfromed.p")
    Obj.train()
    #Obj.predict()  
    Obj.test()  