import numpy as np
from random import randint
import cPickle
from Globals import *
from math import floor
from sklearn.decomposition import PCA
import gc
import glob
from sklearn.datasets import fetch_mldata
import scipy.misc

class MNIST():
    def __init__(self):

        self.Image_size = 32                                                                #CIFAR Images has Images of size 32X32 only
        self.Patch_size = PATCH_SIZE
        self.num_of_rand_patches = NO_OF_RANDOM_PATCHES
        self.colours = COLOURS
        self.Total_classes = 10                                                            #CBLC Datset has faces and non faces
        self.patch_set = np.empty((0,self.colours*self.Patch_size*self.Patch_size),int)                #To collect the patches extracted
        self.resultPath = PATH
        self.Whitten = WHITTEN
        self.count = -1
        self.datasetpath = "/home/vignesh/Datasets/MNIST/"
        self.ImageDim = np.array([self.Image_size,self.Image_size])
        self.input_dataset,self.output_labels = self._readDataset()                         #Reads the data from the binary files


    def _chooseRandomImage(self):
        """Gets a Random Image from the dataset"""

        self.count += 1
        temp = self.count % 59999
        #temp = randint(0,59999)
        return self.input_dataset[temp][:].reshape(self.Image_size,self.Image_size,1), self.output_labels[temp]

    def _chooseTestImage(self,whitten=False):
        """Choose an image from the test Dataset"""

        if(self.count == -1):
            #Invoking this function for the first time, then we will read the test dataset and store it in a variable
            self.testInput,self.testOutput = self._readTestSet()

        self.count += 1
        temp = self.count % self.testInput.shape[0]
        return self.testInput[temp][:].reshape(self.Image_size,self.Image_size,1),self.testOutput[temp]

    def _readDataset(self):
        """Reads the training data set form cPickle file"""

        fileObject = open(self.datasetpath+str('train.p'), 'rb')
        images = cPickle.load(fileObject)
        fileObject.close()

        return images['input'],images['output']

    def _readTestSet(self):
        """Reads the test data set form cPickle file"""
        fileObject = open(self.datasetpath+str('test.p'), 'rb')
        images = cPickle.load(fileObject)
        fileObject.close()

        return images['input'],images['output']


    def readImages(self):
        """Reads the images and preprocess them"""
        mnist = fetch_mldata('MNIST original', data_home=self.datasetpath)
        print 'fetched'
        processedImages = self.processImages(mnist.data)
        print 'processed'
        train = {}
        train['input'] = processedImages[:60000]
        train['output'] = mnist.target[:60000]
        self.writeDatasetInHDD('train.p',train)
        print 'train done'
        del (train)
        test = {}
        test['input'] = processedImages[60000:]
        test['output'] = mnist.target[60000:]
        self.writeDatasetInHDD('test.p',test)
        del (test)
        del(processedImages)
        print 'test done'

        return None


    def writeDatasetInHDD(self, filename,x):
        """Write the dataset variable in HDD"""

        fileobject = open(self.datasetpath+str(filename),'w')
        cPickle.dump(x,fileobject,protocol = 2)                           #Write in protocol 2 since its faster
        fileobject.close()
        return None

    def processImages(self,set):
        """Resize, vectorize and return as a matrix"""
        Dataset = np.empty((0,self.Image_size**2),float)
        count = 0
        for im in set:                        #Read all files in the folder as a list and loops over each element
            Image = scipy.misc.imresize(im.reshape(28,28), self.ImageDim).flatten() #Resize the image to the desired dimension
            count += 1
            print count
            Dataset = np.vstack((Dataset,Image))
        return Dataset


# if __name__ == "__main__":
#
#     Obj = MNIST()
#     Obj.readImages()

