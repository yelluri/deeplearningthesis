import numpy as np
from random import randint
from Globals import *
from math import floor
import cPickle

class Kaggle2():
    def __init__(self):
        
        self.Image_size = IMAGE_SIZE                                                                
        self.Patch_size = PATCH_SIZE
        self.colours = COLOURS
        self.critical_aspect_ratio = self.Patch_size / float(self.Image_size)
        self.num_of_rand_patches = NO_OF_RANDOM_PATCHES
        self.KMeans = K_MEANS
        self.patch_set = np.empty((0,3*self.Patch_size*self.Patch_size),int)                #To collect the patches extracted
        self.col_wise_total = floor(self.Image_size/self.Patch_size)                        #No. of patches possible to extract in a column
        self.row_wise_total = floor(self.Image_size/self.Patch_size)                        #No. of patches possible to extract in a row
        self.total_possible_patches = int(self.col_wise_total * self.row_wise_total)        #Total No. of patches that can be extracted from the image
        self.resultPath = PATH
        self.label = np.array(['cat','dog'])                          
        self.count = 0
        self.datasetpath = "/home/vignesh/Datasets/CatDog_Dict/"
        self.dictdata = self.readDataset()
        
     
    def _chooseRandomImage(self):
        """Chooses an Image at random from the dataset"""
          
        if(self.count > 500):
            self.dictdata = self.readDataset()
            self.count = 0
        Image = self.readImage()
        self.count = self.count + 1
        #print Image.shape[:2]
        if(self.colours == 1):
            return np.dot(Image, [0.299, 0.587, 0.144]).reshape(Image.shape[0],Image.shape[1],self.colours)
        return Image
      
    def readDataset(self):
        """Reads the cpickle file from the HDD"""
        fileobject = open(self.datasetpath+"dataset"+str(randint(0,99))+".p",'rb')
        dic = cPickle.load(fileobject)
        fileobject.close()
          
        return dic
      
    def readImage(self):
        """Reads the image in the dictionary and returns it as a matrix"""
          
        Image = self.dictdata[randint(0,248)]
        dim = Image[0:3]
        Image = Image[3:].reshape(dim)
  
        return Image
    
        
        
    
#     def _readImage(self, filename):
#         """Reads the image from the given path and returns the image as a matrix"""
#           
#         Image = scipy.misc.imread(filename)
#         Image = self._resizeImage(Image)
#           
#         return Image
#       
#       
#     def _resizeImage(self, Image):
#         """Resizes the given Image for having a normalized dimension image with same aspect ratio"""
#   
#         dim = Image.shape
#         newDim = [0,0]
#   
#         if (dim[0]>dim[1]):                                                                 #dim0 is higher
#               
#             ratio = dim[1] / float(dim[0])
#             if (ratio < self.critical_aspect_ratio):
#                   
#                 ratio = self.critical_aspect_ratio
#             newDim[0] = self.Image_size
#             newDim[1] = int(self.Image_size * ratio)
#               
#         else:                                                                               #dim1 is higher
#               
#             ratio = dim[0] / float(dim[1])
#             if (ratio < self.critical_aspect_ratio):
#                 ratio = self.critical_aspect_ratio
#             newDim[0] = int(self.Image_size * ratio)
#             newDim[1] = self.Image_size 
#           
#         resizedImage = scipy.misc.imresize(Image,newDim)
#           
#         return resizedImage
#       
#       
#     def _chooseRandomImage(self):
#         """Chooses an Image at random from the dataset"""
#           
#         flag = True
#         while(flag):                                                                        # To check whether the image is highly rectangular 
#                                                                                             #so that we can't get a patch
#             fileName = "/home/vignesh/Datasets/Cats_Dogs/"+str(self.label[randint(0,1)])+"."+str(randint(0,12499))+".jpg"
#             Image = self._readImage(fileName)
#             if (self.critical_aspect_ratio < Image.shape[0]/float(Image.shape[1]) < 1.0/self.critical_aspect_ratio):
#                 flag = False
#   
#         return Image

# if __name__ == "__main__":
#     Obj = Kaggle2()
#     print "Hello"
#     print Obj._chooseRandomImage().shape        
        
        
      