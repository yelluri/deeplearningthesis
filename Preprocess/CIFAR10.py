import numpy as np
from random import randint
import cPickle
from Globals import *
from math import floor
from sklearn.decomposition import PCA

class CIFAR10():
    def __init__(self):
        
        self.Image_size = 32                                                                #CIFAR Images has Images of size 32X32 only
        self.Patch_size = PATCH_SIZE
        self.num_of_rand_patches = NO_OF_RANDOM_PATCHES
        self.colours = COLOURS
        self.KMeans = K_MEANS
        self.Total_classes = 10                                                             #CIFAR10 Datset has 10 Different classes in total                     
        self.patch_set = np.empty((0,3*self.Patch_size*self.Patch_size),int)                #To collect the patches extracted
        self.col_wise_total = floor(self.Image_size/self.Patch_size)                        #No. of patches possible to extract in a column
        self.row_wise_total = floor(self.Image_size/self.Patch_size)                        #No. of patches possible to extract in a row
        self.total_possible_patches = int(self.col_wise_total * self.row_wise_total)        #Total No. of patches that can be extracted from the image
        self.resultPath = PATH
        self.Whitten = WHITTEN
        self.input_dataset,self.output_labels = self._readDataset()                         #Reads the data from the binary files
        self.count = -1
        #self.whitten()

    
    
    def _chooseRandomImage(self):
        """Gets a Random Image from the dataset"""
        
        self.count += 1
        temp = self.count % 50000
        #temp = randint(0,49999)
        if (self.Whitten == True):
            return self.input_dataset[temp][:].reshape(32,32,self.colours), self.output_labels[temp]
        if (self.colours == 1):
            return np.dot(self.input_dataset[temp][:].reshape(32,32,3), [0.299, 0.587, 0.144]).reshape(32,32,self.colours), self.output_labels[temp]  #Convert to Grey scale
        else:
            return self.input_dataset[temp][:].reshape(32,32,3), self.output_labels[temp]



    
    def writedataset(self):
        """Write the grey scale images to HDD"""
        fileobject = open("/home/vignesh/Datasets/cifar-10-batches-py/CIFAR10Grey",'w')
        cPickle.dump(self.input_dataset,fileobject,protocol = 2)                                   #Stores the patches in cPickle file
        fileobject.close()
        return None
    
    def readGreyImages(self):
        """Read grey scale images from HDD"""
        fileObject = open("/home/vignesh/Datasets/cifar-10-batches-py/CIFAR10Grey", 'rb')
        dataset = cPickle.load(fileObject)
        fileObject.close()
        return dataset
        

        
    def _readDataset(self):
        """"Reads the Images which are stored as vectors in the binary file"""
        
        if (self.Whitten == True):
            """Read the whittened images from HDD"""
            
            fileObject = open("/home/vignesh/Datasets/cifar-10-batches-py/ZWGreytrain.p", 'rb')
            images = cPickle.load(fileObject)
            fileObject.close()
            
            return images['input'],images['output']
            
        else:
            
            for i in xrange(5):                                                                 #There are 5 binary files to fetch the data
                fileObject = open("/home/vignesh/Datasets/cifar-10-batches-py/data_batch_"+str(i+1), 'rb')
                images = cPickle.load(fileObject)
                fileObject.close()
                if (i == 0):
                    inputs_x = np.array(images['data'])                                         #Converting Dictionary to Numpy array for ease in manipulation.
                    labels_y = np.array(images['labels'])
                    imagenames = np.array(images['filenames'])
                else:
                    inputs_x = np.vstack((inputs_x,np.array(images['data'])))                   
                    labels_y = np.concatenate((labels_y,np.array(images['labels'])))
                    imagenames = np.concatenate((imagenames,np.array(images['filenames'])))
            dataset = np.swapaxes(np.swapaxes(inputs_x.reshape(50000,3,32,32),1,2),2,3).reshape(50000,3072)*1.0 #Multiply by 1.0 to make the data as float
            return dataset, labels_y

    def whitten(self,test=False):
        """Zero mean Data with unit Variance"""

        PCAObj = PCA(whiten=True)
        self.input_dataset = PCAObj.fit_transform(self.input_dataset)
        if(test == True):
            self.testInput = PCAObj.transform(self.testInput)

        return None

    def _chooseTestImage(self,whitten=False):
        """Choose an image from the test Dataset"""

        if(self.count == -1):
            #Invoking this function for the first time, then we will read the test dataset and store it in a variable
            self.testInput,self.testOutput = self._readTestSet()

        self.count += 1
        temp = self.count % 10000
        if(self.colours == 1):
            if(self.Whitten == True):
                return self.testInput[temp][:].reshape(32,32,self.colours), self.testOutput[temp]
            else:
                return np.dot(self.testInput[temp][:].reshape(32,32,3), [0.299, 0.587, 0.144]).reshape(32,32,self.colours), self.testOutput[temp]  #Convert to Grey scale
        else:
            return self.testInput[temp][:].reshape(32,32,3),self.testOutput[temp]

    def _readTestSet(self):
        """Reads the test set of CIFAR10"""

        if(self.Whitten == True):
            fileObject = open("/home/vignesh/Datasets/cifar-10-batches-py/ZWGreytest.p", 'rb')
            images = cPickle.load(fileObject)
            fileObject.close()
            dataset, labels_y = images['input'],images['output']

        else:
            fileObj = open('/home/vignesh/Datasets/cifar-10-batches-py/test_batch','rb')
            images = cPickle.load(fileObj)
            fileObj.close()
            inputs_x = np.array(images['data'])                                         #Converting Dictionary to Numpy array for ease in manipulation.
            labels_y = np.array(images['labels'])
            dataset = np.swapaxes(np.swapaxes(inputs_x.reshape(10000,3,32,32),1,2),2,3).reshape(10000,3072)

        del(images)

        return dataset, labels_y