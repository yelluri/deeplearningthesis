'''
Created on Feb 29, 2016

@author: vignesh
'''

import numpy as np
from Globals import *
from Preprocess import CIFAR10,FileManager,Kaggle2,KMAXOIDS,KMEANS
import cPickle
import scipy.misc
from sklearn.preprocessing import MinMaxScaler,StandardScaler
import gc
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report
from scipy.spatial.distance import cdist
import sklearn.metrics.pairwise as pair_dist
import time
class Model2():
    '''
    This Class is used to convert the set of images into set of convex combinations of archetypes
    The archetypes were found at patch size in a hierarchical fashion
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.fileObj = FileManager.FileManager()
        self.resultPath = PATH
        self.Base_Patch_size = PATCH_SIZE
        self.colours = COLOURS
        self.KMeans = K_MEANS
        self.datasetObj = DATASETOBJ
        self.Image_size = IMAGE_SIZE
        self.means = self.fileObj.readAllMeans(self.Image_size*2)
        self.meanImages = self.fileObj.readAllMeanImages(self.Image_size*2)
        self.Total_classes = self.datasetObj.Total_classes
        self.Visualize = VISUALIZE
        self.computeZtZ()                                                               #Compute once and use it multiple times, saves a lot of computation in FW algorithm



    def convertToFeatureVector(self,Image):
        """Converts the given image into feature vectors with learned archetypes"""

        newImage = 0
        PatchSize = self.Base_Patch_size

        #For cropping unwanted pixels
        dim = Image.shape
        OrgImage = Image[0:(0+(dim[0]/PatchSize)*PatchSize),0:(0+(dim[1]/PatchSize)*PatchSize),:]

        dim = OrgImage.shape
        patches = np.swapaxes(OrgImage.reshape(dim[0]/PatchSize,PatchSize,dim[1]/PatchSize,PatchSize,self.colours),1,2).reshape((dim[0]/PatchSize)*(dim[1]/PatchSize),PatchSize,PatchSize,self.colours)
        patches = patches.reshape((dim[0]/PatchSize)*(dim[1]/PatchSize),(PatchSize**2)*self.colours)

        #Reconstructing image with the closest means
        X = self.FrankWolfeReconstruction(self.means[PatchSize].T, patches.T, self.Base_Patch_size)    #Column stockhastic matrix

        if(self.Visualize == True):
            newImage = np.dot(self.means[PatchSize].T,X).T
            newImage = np.swapaxes(newImage.reshape((dim[0]/PatchSize)*(dim[1]/PatchSize),PatchSize,PatchSize,self.colours).reshape((dim[0]/PatchSize),(dim[1]/PatchSize),PatchSize,PatchSize,self.colours),1,2)
            newImage = newImage.reshape((dim[0]/PatchSize)*PatchSize,(dim[1]/PatchSize)*PatchSize,self.colours)

        WImage = np.swapaxes(X,1,0).reshape(dim[0]/PatchSize,dim[1]/PatchSize,self.KMeans[PatchSize])
        #For reconstruction in the furthur levels
        while(PatchSize < self.Image_size):
            PatchSize = 2 * PatchSize

            if(PatchSize == self.Image_size*2):                                         #For final layer, no need to do pooling
                OrgImage = WImage
            else:
                #For cropping unwanted pixels
                newDim = WImage.shape
                OrgImage = WImage[0:(0+(newDim[0]/2)*2),0:(0+(newDim[1]/2)*2),:]

            #For collecting the set of patches to do reconstruction for each patch
            newDim = OrgImage.shape

            if(PatchSize == self.Image_size*2):                                         #For final layer, no need to do pooling
                patches = OrgImage.reshape(1,OrgImage.flatten().shape[0])
            else:
                patches = np.swapaxes(OrgImage.reshape(newDim[0]/2,2,newDim[1]/2,2,self.KMeans[PatchSize/2]),1,2).reshape((newDim[0]/2)*(newDim[1]/2),2,2,self.KMeans[PatchSize/2])
                patches = patches.reshape((newDim[0]/2)*(newDim[1]/2),(2*2)*self.KMeans[PatchSize/2])

            #For computing pairwise patch reconstruction
            X = self.FrankWolfeReconstruction(self.means[PatchSize].T, patches.T, PatchSize)        #Column stockhastic matrix
            if(self.Visualize == True):
                newImage = np.dot(self.meanImages[PatchSize].T,X).T
                newImage = np.swapaxes(newImage.reshape((dim[0]/PatchSize)*(dim[1]/PatchSize),PatchSize,PatchSize,self.colours).reshape((dim[0]/PatchSize),(dim[1]/PatchSize),PatchSize,PatchSize,self.colours),1,2)
                newImage = newImage.reshape((dim[0]/PatchSize)*PatchSize,(dim[1]/PatchSize)*PatchSize,self.colours)

            if(PatchSize == self.Image_size*2):                                         #For final layer, no need to do pooling
                WImage = X
            else:
                WImage = np.swapaxes(X,1,0).reshape(newDim[0]/2,newDim[1]/2,self.KMeans[PatchSize])

        return newImage,WImage

    def computeZtZ(self):
        """Computes the Z.T * Z matrix once for Frank Wolfe Algorithm"""
        self.ZtZ = {}
        for i in self.means.keys():
            self.ZtZ[i] = np.dot(self.means[i],self.means[i].T)
        return None


    def FrankWolfeReconstruction(self, Z, X, PatchSize, tmax = 100):
        """Perform Constrained Convex optimization using frank wolfe Algorithm for image reconstruction"""

        k = self.KMeans[PatchSize]
        n = X.shape[1]

        # helper matrix of all zeros
        O = np.zeros((k,n))

        # initial guess for coefficients
        A = np.ones((k,n)); A /= k


        # compute ingredients of gradient of E
        #ZtZ = np.dot(Z.T, Z)
        ZtX = np.dot(Z.T, X)


        k, n = A.shape
        inds = np.arange(n)
        for t in range(tmax):
            gA = np.dot(self.ZtZ[PatchSize], A) - ZtX
            mininds = np.argmin(gA, axis=0)
            O[mininds,inds] = 1.
            a = 2./(t+2)
            A = A + a * (O - A)
            O[mininds,inds] = 0.
        return A




    def savebyCpickle(self,data,filename):
        """Write data into a Cpickle file in HDD"""

        fileobject = open(str(self.resultPath)+filename,'wb')
        cPickle.dump(data,fileobject,protocol = 2)                                   #Stores the patches in cPickle file
        fileobject.close()
        return None


    def readCpickleFile(self, filename):
        """Read a Cpickle from HDD"""

        fileobj = open(str(self.resultPath)+filename,'rb')
        data = cPickle.load(fileobj)
        fileobj.close()
        return data

    def ScaleData(self, trainX, testX, Archetypes, Type = 'MinMax'):
        """Scale the Data"""

        if(Type == 'MinMax'):
            Scaler = MinMaxScaler(feature_range=(-1,1))
        elif(Type == 'Whitten'):
            Scaler = StandardScaler()
        print trainX.shape,Archetypes.shape,testX.shape
        trainX = Scaler.fit_transform(trainX)
        testX = Scaler.transform(testX)
        Archetypes = Scaler.transform(Archetypes)

        return trainX,testX,Archetypes

    def getDataset(self, Type = 'Means', ScalingType = 'Whitten'):
        """Returns the training and test Dataset after Scaling it as Scecified
        Type can be 'Means', 'Images'
        Scale Types are: MinMax, Whitten
        """
        if(Type == 'Means'):

            trainSet = self.readCpickleFile("train.p")
            self.trainX = trainSet['input']
            self.trainY = trainSet['output'].flatten()
            del(trainSet)
            testSet = self.readCpickleFile("test.p")
            self.testX = testSet['input']
            self.testY = testSet['output'].flatten()
            del(testSet)
            self.Archetypes = self.means[self.Image_size *2]
            #self.trainX,self.testX, self.Archetypes = self.ScaleData(self.trainX, self.testX, self.Archetypes, ScalingType)

        else:
            self.trainX,self.trainY = self.datasetObj._readDataset()
            self.testX, self.testY = self.datasetObj._readTestSet()
            gc.collect()
            if(DATASET == 'CIFAR10'):
                if(WHITTEN == False):
                    self.trainX = np.dot(self.trainX.reshape(50000,32,32,3),[0.299, 0.587, 0.144]).reshape(50000,1024)
                    self.testX = np.dot(self.testX.reshape(10000,32,32,3),[0.299, 0.587, 0.144]).reshape(10000,1024)
                else:
                    self.trainX = self.trainX.reshape(50000,32,32,1).reshape(50000,1024)
                    self.testX = self.testX.reshape(10000,32,32,1).reshape(10000,1024)
            self.Archetypes = self.meanImages[self.Image_size *2]
            #self.trainX,self.testX, self.Archetypes = self.ScaleData(self.trainX, self.testX, self.Archetypes, ScalingType)
        if(DATASET == 'CBLC'):
            self.trainY[np.where(self.trainY== -1.0)] = 0.0
            self.testY[np.where(self.testY == -1.0)] = 0.0
            print np.unique(self.trainY),np.unique(self.testY)
        print self.trainX.shape,self.testX.shape
        return None
    def LabelledArchetypes(self):
        """to see the archetypes classwise"""
        for i in xrange(self.Total_classes):
            ind = np.where(self.ArchLabels == (i*1.0))
            images = self.meanImages[self.Image_size*2][ind]
            path = self.resultPath+'classes/'+str(i)
            self.fileObj.validateFilePath(path)
            for j in xrange(images.shape[0]):
                self.fileObj.saveImage(path+'/patch '+str(j)+'.jpg', self.fileObj.resizeImage(images[j].reshape(self.Image_size,self.Image_size,self.colours),(50,50)))

        return None
    def LabelArchetypes(self):
        """Label the Archetypes so that, we can use them to classify test data"""
        self.ArchLabels = self.knn(1,self.trainX,self.trainY,self.Archetypes).flatten()
        #self.LabelledArchetypes()
        print "Labelled the Archetypes"
        return None

    def perdict(self):
        """Based on the labelled Archetypes, classify the test points"""
        #predictY = self.knn(10,self.Archetypes,self.ArchLabels,self.testX)

        distMatrix = pair_dist.pairwise_distances(self.Archetypes, self.testX,metric='euclidean',n_jobs=-1)

        self.classWiseDistances = np.empty((0,self.testX.shape[0]))
        for i in xrange(self.Total_classes):
            indices = np.array(np.where(self.ArchLabels == i)).flatten()                            #Indices where the class is 'i'
            print indices.shape,i
            classSum = np.sum(distMatrix[indices],axis=0) * 1.0/(indices.shape[0])                 #Normalizing the class sum
            self.classWiseDistances = np.vstack((self.classWiseDistances,classSum))
        #Now we have the classwise sum of distances for all test pointsin self.classWiseDistances, with this we should classify
        print self.classWiseDistances.shape
        predictY = np.argmin(self.classWiseDistances,axis = 0)
        print predictY
        print classification_report(self.testY,predictY)
        return None


    def knn(self,k,trainX,trainY,testX):
        """Train and predict with Knn classifier"""

        KnnObj = KNeighborsClassifier(n_neighbors=k,n_jobs = -1)
        KnnObj.fit(trainX, trainY)
        return KnnObj.predict(testX)





if __name__ == "__main__":

    Obj = Model2()
    Obj.getDataset('Images')
    Obj.LabelArchetypes()
    Obj.perdict()