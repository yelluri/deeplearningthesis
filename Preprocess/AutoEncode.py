import numpy as np
from Globals import *


class AutoEncode():
    def __init__(self, maxoids = 10):

        self.count = 0
        self.k = maxoids

    def train(self, X):

        if(X.ndim>1):
            for x in X:
                self.onlinekMaxoids(x)
        else:
            self.onlinekMaxoids(X)

        return None

    def onlinekMaxoids(self,x):

        if(self.count < self.k):                                        #Initialising the centroids until the number of clusters
            if(self.count == 0):
                self.M = x
            else:
                self.M = np.vstack((self.M, x))
        else:

            D = np.sum((self.M - x)**2,axis=1)
            i = np.argmin(D)
            d = np.sum((self.M - self.M[i,:])**2,axis=1)

            D[i] = 0.
            d[i] = 0.
            px = np.sum(D)
            pm = np.sum(d)

            if px > pm:
                self.M[i,:] = x
        self.count = self.count + 1

        return None

    def getCentroids(self):
        return self.M

    def encode(self, X, tmax = 1000):
        """This method transforms the given data into co-effcients of Maxoids"""

        X = X.T
        n = X.shape[1]
        Z = self.getCentroids().T

        # helper matrix of all zeros
        O = np.zeros((self.k,n))

        # initial guess for coefficients
        A = np.ones((self.k,n)); A /= self.k


        # compute ingredients of gradient of E
        ZtZ = np.dot(Z.T, Z)
        ZtX = np.dot(Z.T, X)

        self.k, n = A.shape
        inds = np.arange(n)
        for t in range(tmax):
            gA = np.dot(ZtZ, A) - ZtX
            mininds = np.argmin(gA, axis=0)
            O[mininds,inds] = 1.
            a = 2./(t+2)
            A = A + a * (O - A)
            O[mininds,inds] = 0.
        return A.T