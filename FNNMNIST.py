import numpy as np
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.optimizers import SGD, Adam, RMSprop
from keras.utils import np_utils
from Dataset import *
from Globals import *
class FNN():
    def __init__(self):
        """Constructor"""

        self.batch_size = 128
        self.nb_classes = DATASETOBJ.Total_classes
        self.nb_epoch = 20


    def train(self):
        trainX, trainY, testX, testY = getDataset(ScalingType='Whitten',ReducedDim=400)
        # trainX,trainY = DATASETOBJ._readDataset()
        # testX,testY = DATASETOBJ._readTestSet()
        # trainX/=255
        # testY/=255
        #testX, testY = trainX,trainY
        # convert class vectors to binary class matrices
        trainY = np_utils.to_categorical(trainY, self.nb_classes)
        testY = np_utils.to_categorical(testY, self.nb_classes)

        model = Sequential()
        model.add(Dense(2, input_shape=(trainX.shape[1],)))
        model.add(Activation('relu'))
        model.add(Dropout(0.2))
        # model.add(Dense(512))
        # model.add(Activation('relu'))
        # model.add(Dropout(0.2))
        model.add(Dense(self.nb_classes))
        model.add(Activation('softmax'))

        rms = RMSprop()
        model.compile(loss='categorical_crossentropy', optimizer=rms)
        # let's train the model using SGD + momentum (how original).
        #sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
        #model.compile(loss='categorical_crossentropy', optimizer=sgd)

        model.fit(trainX, trainY,
                  batch_size=self.batch_size, nb_epoch=self.nb_epoch,
                  show_accuracy=True, verbose=2,
                  validation_data=(testX, testY))
        score = model.evaluate(testX, testY,
                               show_accuracy=True, verbose=0)
        print('Test score:', score[0])
        print('Test accuracy:', score[1])

        return None


if __name__ == "__main__":

    Obj = FNN()
    Obj.train()