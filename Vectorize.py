import numpy as np
from random import randint
import scipy.misc
from Globals import *
from math import floor
import time
import cPickle
from Preprocess import FileManager

class Vectorize():
    def __init__(self):
        
        self.Image_size = IMAGE_SIZE                                                                
        self.Patch_size = PATCH_SIZE
        self.critical_aspect_ratio = 3.0
        self.num_of_rand_patches = NO_OF_RANDOM_PATCHES
        self.KMeans = K_MEANS
        self.patch_set = np.empty((0,3*self.Patch_size*self.Patch_size),int)                #To collect the patches extracted
        self.col_wise_total = floor(self.Image_size/self.Patch_size)                        #No. of patches possible to extract in a column
        self.row_wise_total = floor(self.Image_size/self.Patch_size)                        #No. of patches possible to extract in a row
        self.total_possible_patches = int(self.col_wise_total * self.row_wise_total)        #Total No. of patches that can be extracted from the image
        self.resultPath = PATH
        self.label = np.array(['cat','dog'])   
        self.count = 0
        self.maxImageLength = (3*int(self.critical_aspect_ratio)*(self.Image_size**2))+3
        self.Image_set = np.empty((0,self.maxImageLength),int)
        self.fileObj = FileManager.FileManager()
        
        
        
    def _readImage(self, filename):
        """Reads the image from the given path and returns the image as a matrix"""
        
        Image = scipy.misc.imread(filename)
        Image = self._resizeImage(Image)
        
        return Image
    
    
    def _resizeImage(self, Image):
        """Resizes the given Image for having a normalized dimension image with same aspect ratio"""

        dim = Image.shape
        newDim = [0,0]

        if (dim[0]<dim[1]):                                                                 #dim0 is higher
            
            ratio = dim[1] / float(dim[0])
#             if (ratio > self.critical_aspect_ratio):
#                 ratio = self.critical_aspect_ratio
            newDim[0] = self.Image_size
            newDim[1] = int(self.Image_size * ratio)
            
        else:                                                                               #dim1 is higher
            
            ratio = dim[0] / float(dim[1])
#             if (ratio > self.critical_aspect_ratio):
#                 ratio = self.critical_aspect_ratio
            newDim[0] = int(self.Image_size * ratio)
            newDim[1] = self.Image_size 
        
        resizedImage = scipy.misc.imresize(Image,newDim)
        
        return resizedImage
    
    
    def _chooseRandomImage(self):
        """Chooses an Image at random from the dataset"""
        
        flag = True
        while(flag):                                                                        # To check whether the image is highly rectangular 
                                                                                            #so that we can't get a patch
            fileName = "/home/vignesh/Datasets/Cats_Dogs/"+str(self.label[self.count/12500])+"."+str(self.count%12500)+".jpg"
            Image = self._readImage(fileName)
            self.count = self.count + 1
            if (self.critical_aspect_ratio > Image.shape[0]/float(Image.shape[1]) > 1.0/self.critical_aspect_ratio):
                flag = False

        return Image
    
    def collectImages(self):
        """Gets an image and stores it in a matrix as a Vector """
        for i in xrange(3000):
            print i
            Image = self._chooseRandomImage()
            dim = np.array(Image.shape)
            zeroPadding = self.maxImageLength-int(np.prod(dim))-3
            zeros = np.zeros(zeroPadding)
            vector = np.concatenate((dim,Image.flatten()),axis = 0)
            vector = np.concatenate((vector,zeros),axis = 0)
            self.Image_set = np.vstack((self.Image_set,vector))
            
#         #assert 0,self.Image_set.shape
#         Image = self.Image_set[0]
#         dim = Image[0:3]
#         Image = Image[3:np.prod(dim)+3]
#         print Image.shape
#         scipy.misc.imsave('test0.jpg', Image.reshape(dim))
#         assert 0,dim
        return None
            
    def collectIm(self):
        """Stores images as a vector in a dictionary"""
        #self.dicimage = {}
        for j in xrange(100):
            self.dicimage = {}
            for i in xrange(249):
                print j,i
                Image = self._chooseRandomImage()
                dim = np.array(Image.shape)
                vector = np.concatenate((dim,Image.flatten()),axis = 0)
                self.dicimage[i]=vector
            self.writeDataset("dataset"+str(j), self.dicimage)
        return None
            
    def writeDataset(self, filename, data):
        """Writes the dictionary in a cpickle file"""
        path = "/home/vignesh/Datasets/CatDog_Dict/"
        self.fileObj.validateFilePath(path)
        fileobject = open(path+filename+".p",'wb')
        cPickle.dump(data,fileobject,protocol = 2)                                   #Stores the patches in cPickle file
        fileobject.close()
        print "Fi"
        return None
    
    
        
if __name__ == "__main__":
    
    Obj = Vectorize()
    t1 = time.time()
    Obj.collectIm()
    print time.time()-t1